use qpdf;
use serde::{Deserialize, Serialize};

mod metadata;
mod page_labels;
mod recogniser;

trait FromPdf
where
    Self: Sized,
{
    type Source;

    fn from_pdf(
        value: &Self::Source,
        warnings: &mut qpdf::Warnings,
    ) -> qpdf::Result<Self>;
}

trait ToPdf {
    type Target;

    fn to_pdf(
        &self,
        doc: &mut qpdf::Document,
        warnings: &mut qpdf::Warnings,
    ) -> qpdf::Result<Self::Target>;
}

#[derive(
    Default, Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize,
)]
pub struct Patch {
    #[serde(default)]
    recogniser: recogniser::Recogniser,
    #[serde(default)]
    metadata: metadata::Metadata,
    page_labels: Option<page_labels::PageLabels>,
}

impl Patch {
    pub fn from_document(
        doc: &qpdf::Document,
        warnings: &mut qpdf::Warnings,
    ) -> qpdf::Result<Patch> {
        let trailer: qpdf::objects::Dict = doc.trailer(warnings)?.try_into()?;
        let root: qpdf::objects::Dict =
            trailer.get(qpdf::keys::ROOT, warnings)?.try_into()?;

        let page_labels_pdf: Option<qpdf::objects::Dict> = root
            .get(qpdf::keys::PAGE_LABELS, warnings)?
            .try_into()
            // If the PageLabels key doesn't have a dict,
            // it's probably missing and not worth complaining about.
            .ok();

        let page_labels = page_labels_pdf.and_then(|value| {
            page_labels::PageLabels::from_pdf(&value, warnings)
                // If the dict can't be converted to a PageLabels,
                // we record it and move on.
                .map_err(|e| warnings.push(e))
                .ok()
        });

        Ok(Patch {
            recogniser: recogniser::Recogniser::from_document(doc, warnings)?,
            metadata: metadata::Metadata::from_document(doc, warnings)?,
            page_labels,
        })
    }

    pub fn recognise(
        &self,
        doc: &qpdf::Document,
        warnings: &mut qpdf::Warnings,
    ) -> qpdf::Result<u32> {
        self.recogniser.recognise(doc, warnings)
    }

    pub fn inject_into(
        &self,
        doc: &mut qpdf::Document,
        warnings: &mut qpdf::Warnings,
    ) -> qpdf::Result<()> {
        self.metadata.inject_into(doc, warnings)?;

        let trailer: qpdf::objects::Dict = doc.trailer(warnings)?.try_into()?;
        let mut root: qpdf::objects::Dict =
            trailer.get(qpdf::keys::ROOT, warnings)?.try_into()?;

        if let Some(ref page_labels) = self.page_labels {
            let page_labels_pdf = page_labels.to_pdf(doc, warnings)?;
            root.set(qpdf::keys::PAGE_LABELS, page_labels_pdf, warnings)?;
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn extract_patch() {
        let mut warnings = vec![];
        let document = qpdf::Document::open(
            "testdata/PDF test document 1.pdf",
            &mut warnings,
        )
        .expect("Couldn't open file");
        let patch = Patch::from_document(&document, &mut warnings)
            .expect("Could not extract patch");

        assert_eq!(patch.recogniser.page_count.unwrap(), 2);
        assert_eq!(patch.recogniser.format.unwrap(), "PDF 1.6");
        assert_eq!(patch.recogniser.author, None);
        assert_eq!(patch.recogniser.title.unwrap(), "PDF test");
        assert_eq!(patch.recogniser.producer.unwrap(), "LibreOffice 7.2");
        assert_eq!(patch.recogniser.creator.unwrap(), "Writer");
        assert_eq!(
            patch.recogniser.subject.unwrap(),
            "Testing of PDF metadata"
        );
        assert_eq!(
            patch.recogniser.keywords.unwrap(),
            "PDF, testing, metadata"
        );
        assert_eq!(patch.metadata.author, None);
        assert_eq!(patch.metadata.title.unwrap(), "PDF test");
        assert_eq!(patch.metadata.producer.unwrap(), "LibreOffice 7.2");
        assert_eq!(patch.metadata.creator.unwrap(), "Writer");
        assert_eq!(
            patch.metadata.creation_date.unwrap(),
            "D:20211203161047+11'00'"
        );
        assert_eq!(patch.metadata.modification_date, None);
        assert_eq!(patch.metadata.subject.unwrap(), "Testing of PDF metadata");
        assert_eq!(patch.metadata.keywords.unwrap(), "PDF, testing, metadata");

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn recognise_self() {
        let mut warnings = vec![];
        let document = qpdf::Document::open(
            "testdata/PDF test document 1.pdf",
            &mut warnings,
        )
        .expect("Couldn't open file");
        let patch = Patch::from_document(&document, &mut warnings)
            .expect("Could not extract patch");

        let score = patch
            .recognise(&document, &mut warnings)
            .expect("Could not recognise document");

        // All the fields in the recogniser should match.
        assert_eq!(score, 8);

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn recognise_other() {
        let mut warnings = vec![];
        let source = qpdf::Document::open(
            "testdata/PDF test document 1.pdf",
            &mut warnings,
        )
        .expect("Couldn't open source");
        let patch = Patch::from_document(&source, &mut warnings)
            .expect("Could not extract patch");
        let target = qpdf::Document::open(
            "testdata/PDF test document 2.pdf",
            &mut warnings,
        )
        .expect("Couldn't open target");

        let score = patch
            .recognise(&target, &mut warnings)
            .expect("Could not recognise document");

        // The subject field is document 2 is different.
        assert_eq!(score, 7);

        assert_eq!(warnings.len(), 0);
    }
}
