use serde::{Deserialize, Serialize};

#[derive(
    Default, Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize,
)]
pub struct Recogniser {
    pub page_count: Option<i64>,
    pub format: Option<String>,
    pub author: Option<String>,
    pub title: Option<String>,
    pub producer: Option<String>,
    pub creator: Option<String>,
    pub subject: Option<String>,
    pub keywords: Option<String>,
}

impl Recogniser {
    pub fn from_document(
        doc: &qpdf::Document,
        warnings: &mut qpdf::Warnings,
    ) -> qpdf::Result<Self> {
        let mut res: Self = Default::default();

        res.format = Some(format!("PDF {}", doc.get_pdf_version(warnings)?));

        let trailer: qpdf::objects::Dict = doc.trailer(warnings)?.try_into()?;
        let maybe_root: Option<qpdf::objects::Dict> =
            trailer.get(qpdf::keys::ROOT, warnings)?.into();
        let maybe_pages: Option<qpdf::objects::Dict> = maybe_root
            .and_then(|root| root.get(qpdf::keys::PAGES, warnings).ok())
            .and_then(Into::into);

        res.page_count = maybe_pages
            .and_then(|pages| pages.get(qpdf::keys::COUNT, warnings).ok())
            .and_then(Into::into);

        let maybe_info: Option<qpdf::objects::Dict> =
            trailer.get(qpdf::keys::INFO, warnings)?.into();

        let mut get_info_key = |key| {
            maybe_info
                .clone()
                .and_then(|info| info.get(key, warnings).ok())
                .and_then(Into::into)
        };

        res.author = get_info_key(qpdf::keys::AUTHOR);
        res.title = get_info_key(qpdf::keys::TITLE);
        res.producer = get_info_key(qpdf::keys::PRODUCER);
        res.creator = get_info_key(qpdf::keys::CREATOR);
        res.subject = get_info_key(qpdf::keys::SUBJECT);
        res.keywords = get_info_key(qpdf::keys::KEYWORDS);

        Ok(res)
    }

    pub fn recognise(
        &self,
        doc: &qpdf::Document,
        warnings: &mut qpdf::Warnings,
    ) -> qpdf::Result<u32> {
        let other = Recogniser::from_document(doc, warnings)?;

        let mut res: u32 = 0;

        macro_rules! update_score {
            ($this:expr, $other:expr) => {
                res += match (&$this, &$other) {
                    (Some(x), Some(y)) => (x == y) as u32,

                    // if the recogniser doesn't require a particular field,
                    // count it as a match, not a miss.
                    (None, _) => 1,

                    // If the recogniser requires a value
                    // and the document doesn't have one,
                    // that's not a match.
                    _ => 0,
                };
            };
        }

        update_score!(self.page_count, other.page_count);
        update_score!(self.format, other.format);
        update_score!(self.author, other.author);
        update_score!(self.title, other.title);
        update_score!(self.producer, other.producer);
        update_score!(self.creator, other.creator);
        update_score!(self.subject, other.subject);
        update_score!(self.keywords, other.keywords);

        Ok(res)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn unspecified_fields_count_as_matches() {
        let mut warnings = vec![];
        let source = qpdf::Document::open(
            "testdata/PDF test document 1.pdf",
            &mut warnings,
        )
        .expect("Couldn't open source");
        let mut recogniser = Recogniser::from_document(&source, &mut warnings)
            .expect("Could not train recogniser");

        // Let's assume we don't actually care about the subject field.
        recogniser.subject = None;

        let target = qpdf::Document::open(
            "testdata/PDF test document 2.pdf",
            &mut warnings,
        )
        .expect("Couldn't open target");
        let score = recogniser
            .recognise(&target, &mut warnings)
            .expect("Could not recognise document");

        // Even though the subject is different, it should count as a match.
        assert_eq!(score, 8);

        assert_eq!(warnings.len(), 0);
    }
}
