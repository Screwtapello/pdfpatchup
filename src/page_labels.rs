use std::collections;
use std::num::NonZeroU32;

use serde::{Deserialize, Serialize};

use crate::FromPdf;
use crate::ToPdf;

const PAGE_LABEL: &str = "PageLabel";
const STYLE: &str = "S";
const DECIMAL: &str = "D";
const UPPERCASE_ROMAN: &str = "R";
const LOWERCASE_ROMAN: &str = "r";
const UPPERCASE_LETTERS: &str = "A";
const LOWERCASE_LETTERS: &str = "a";
const PREFIX: &str = "P";
const START: &str = "St";

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Style {
    Decimal,
    UppercaseRoman,
    LowercaseRoman,
    UppercaseLetters,
    LowercaseLetters,
}

impl FromPdf for Style {
    type Source = qpdf::objects::Name;

    fn from_pdf(
        value: &Self::Source,
        _warnings: &mut qpdf::Warnings,
    ) -> qpdf::Result<Self> {
        match value {
            // We can't use DECIMAL => Style::Decimal
            // because `match` won't use PartialEq to compare values.
            // However, we can use match guards to compare values.
            x if x == DECIMAL => Ok(Style::Decimal),
            x if x == UPPERCASE_ROMAN => Ok(Style::UppercaseRoman),
            x if x == LOWERCASE_ROMAN => Ok(Style::LowercaseRoman),
            x if x == UPPERCASE_LETTERS => Ok(Style::UppercaseLetters),
            x if x == LOWERCASE_LETTERS => Ok(Style::LowercaseLetters),
            _ => Err(qpdf::Error {
                filename: Default::default(),
                position: 0,
                kind: qpdf::error::ErrorKind::Object,
                message: format!("Not a valid style name: {:?}", value),
            }),
        }
    }
}

impl ToPdf for Style {
    type Target = qpdf::objects::Name;

    fn to_pdf(
        &self,
        doc: &mut qpdf::Document,
        warnings: &mut qpdf::Warnings,
    ) -> qpdf::Result<Self::Target> {
        let data = match self {
            Style::Decimal => DECIMAL,
            Style::UppercaseRoman => UPPERCASE_ROMAN,
            Style::LowercaseRoman => LOWERCASE_ROMAN,
            Style::UppercaseLetters => UPPERCASE_LETTERS,
            Style::LowercaseLetters => LOWERCASE_LETTERS,
        };

        Ok(qpdf::objects::Name::new(data, doc, warnings)?)
    }
}

#[derive(
    Debug, Default, Clone, Hash, PartialEq, Eq, Serialize, Deserialize,
)]
pub struct LabelFormat {
    style: Option<Style>,
    prefix: Option<String>,
    start: Option<NonZeroU32>,
}

impl FromPdf for LabelFormat {
    type Source = qpdf::objects::Dict;

    fn from_pdf(
        value: &Self::Source,
        warnings: &mut qpdf::Warnings,
    ) -> qpdf::Result<Self> {
        match value.get(qpdf::keys::TYPE, warnings)? {
            qpdf::objects::Value::Null(_) => {
                // Type key is missing, nevermind.
            }
            qpdf::objects::Value::Name(x) if &x == PAGE_LABEL => {
                // Yes, the dict type is correct!
            }
            x => {
                // Thi is not the right kind of dict.
                return Err(qpdf::Error {
                    filename: Default::default(),
                    position: 0,
                    kind: qpdf::error::ErrorKind::Object,
                    message: format!("This is not a PageLabel dict: {:?}", x),
                });
            }
        }

        let style: Option<Style> = match value.get(STYLE, warnings)? {
            qpdf::objects::Value::Null(_) => None, // Missing key, that's fine.
            qpdf::objects::Value::Name(name) => {
                // If we've got a valid style, use it
                Style::from_pdf(&name, warnings).ok()
            }
            other => {
                // It's not a name,
                // let's grumble and continue.
                warnings.push(qpdf::Error {
                    filename: Default::default(),
                    position: 0,
                    kind: qpdf::error::ErrorKind::Object,
                    message: format!(
                        "PageLabel style should be a Name: {:?}",
                        other,
                    ),
                });
                None
            }
        };

        let prefix: Option<String> = match value.get(PREFIX, warnings)? {
            // Missing key, that's fine.
            qpdf::objects::Value::Null(_) => None,

            // If the value is a valid text string, use it.
            qpdf::objects::Value::String(ref s) => {
                s.try_into().map_err(|e| warnings.push(e)).ok()
            }
            other => {
                // It's not a string,
                // let's grumble and continue.
                warnings.push(qpdf::Error {
                    filename: Default::default(),
                    position: 0,
                    kind: qpdf::error::ErrorKind::Object,
                    message: format!(
                        "PageLabel prefix should be a String: {:?}",
                        other,
                    ),
                });
                None
            }
        };

        let start: Option<NonZeroU32> = match value.get(START, warnings)? {
            // Missing key, that's fine.
            qpdf::objects::Value::Null(_) => None,
            qpdf::objects::Value::Integer(ref integer) => {
                let rust_integer: i64 = integer.try_into()?;
                if 0 < rust_integer && rust_integer <= (u32::MAX as i64) {
                    // Start is a sensible number, use it
                    NonZeroU32::new(rust_integer as u32)
                } else {
                    // Start is a wildly non-sensical number,
                    // let's grumble and continue
                    warnings.push(qpdf::Error {
                        filename: Default::default(),
                        position: 0,
                        kind: qpdf::error::ErrorKind::Object,
                        message: format!(
                            "PageLabel start should be positive, not {:?}",
                            rust_integer,
                        ),
                    });
                    None
                }
            }
            other => {
                // It's not an integer,
                // let's grumble and continue.
                warnings.push(qpdf::Error {
                    filename: Default::default(),
                    position: 0,
                    kind: qpdf::error::ErrorKind::Object,
                    message: format!(
                        "PageLabel start should be an Integer: {:?}",
                        other,
                    ),
                });
                None
            }
        };

        Ok(LabelFormat {
            style,
            prefix,
            start,
        })
    }
}

impl ToPdf for LabelFormat {
    type Target = qpdf::objects::Dict;

    fn to_pdf(
        &self,
        doc: &mut qpdf::Document,
        warnings: &mut qpdf::Warnings,
    ) -> qpdf::Result<Self::Target> {
        let mut dict = qpdf::objects::Dict::new(doc, warnings)?;

        dict.set(
            qpdf::keys::TYPE,
            qpdf::objects::Name::new(PAGE_LABEL, doc, warnings)?,
            warnings,
        )?;
        if let Some(style) = self.style {
            dict.set(STYLE, style.to_pdf(doc, warnings)?, warnings)?;
        }
        if let Some(ref prefix) = self.prefix {
            dict.set(
                PREFIX,
                qpdf::objects::String::from_text(prefix, doc, warnings)?,
                warnings,
            )?;
        }

        if let Some(start) = self.start {
            dict.set(
                START,
                qpdf::objects::Integer::new(start.get().into(), doc, warnings)?,
                warnings,
            )?;
        }

        Ok(dict)
    }
}

type PageLabelMap = collections::BTreeMap<u32, LabelFormat>;

#[derive(Debug, Default, Clone, Hash, PartialEq, Eq)]
pub struct PageLabels(PageLabelMap);

fn extract_page_tree_node(
    node: qpdf::objects::Dict,
    warnings: &mut qpdf::Warnings,
) -> qpdf::Result<(Vec<qpdf::objects::Dict>, PageLabelMap)> {
    let mut res_kids = vec![];
    let mut res_labels = PageLabelMap::new();

    let maybe_kids: qpdf::Result<qpdf::objects::Array> =
        node.get(qpdf::keys::KIDS, warnings)?.try_into();
    if let Ok(kids_array) = maybe_kids {
        let kids_vec_values: Vec<qpdf::objects::Value> =
            (&kids_array).try_into()?;
        let maybe_kids_vec_dicts: qpdf::Result<Vec<qpdf::objects::Dict>> =
            kids_vec_values.into_iter().map(TryInto::try_into).collect();
        res_kids.extend(maybe_kids_vec_dicts?);
    }

    let maybe_nums: qpdf::Result<qpdf::objects::Array> =
        node.get(qpdf::keys::NUMS, warnings)?.try_into();
    if let Ok(nums_array) = maybe_nums {
        let mut nums: Vec<qpdf::objects::Value> = (&nums_array).try_into()?;

        while nums.len() > 0 {
            // we can unwrap, because we already checked the length.
            let value = nums.pop().unwrap();
            let value: qpdf::objects::Dict = value.try_into()?;
            let value = LabelFormat::from_pdf(&value, warnings)?;

            // If there's not also a key,
            // there must have been an odd number of items.
            let key = nums.pop().ok_or_else(|| qpdf::Error {
                filename: Default::default(),
                position: 0,
                kind: qpdf::error::ErrorKind::Object,
                message: format!(
                    "PageLabels array must have an even number of items"
                ),
            })?;
            let key: i64 = key.try_into()?;
            let key: u32 = key.try_into().map_err(|e| qpdf::Error {
                filename: Default::default(),
                position: 0,
                kind: qpdf::error::ErrorKind::Object,
                message: format!(
                    "Page index {:?} cannot fit into u32: {:?}",
                    key, e
                ),
            })?;

            res_labels.insert(key, value);
        }
    }

    Ok((res_kids, res_labels))
}

impl FromPdf for PageLabels {
    type Source = qpdf::objects::Dict;

    fn from_pdf(
        value: &Self::Source,
        warnings: &mut qpdf::Warnings,
    ) -> qpdf::Result<Self> {
        let mut res = PageLabelMap::new();
        let mut checking = vec![value.clone()];
        let mut to_check = vec![];

        while checking.len() > 0 {
            for node in checking.drain(..) {
                let (new_to_check, new_labels) =
                    extract_page_tree_node(node, warnings)?;
                to_check.extend(new_to_check.into_iter());
                res.extend(new_labels.into_iter());
            }
            // FIXME: This code will get stuck on graph cycles,
            // but QPDF does not seem to provide any way to check whether
            // two object handles represent the same underlying object
            // so we can't detect cycles ourselves.
            checking.extend(to_check.drain(..));
        }

        Ok(PageLabels(res))
    }
}

impl ToPdf for PageLabels {
    type Target = qpdf::objects::Dict;

    fn to_pdf(
        &self,
        doc: &mut qpdf::Document,
        warnings: &mut qpdf::Warnings,
    ) -> qpdf::Result<Self::Target> {
        let mut nums = qpdf::objects::Array::new(doc, warnings)?;

        for (key, value) in self.0.iter() {
            nums.push(
                qpdf::objects::Integer::new(*key as _, doc, warnings)?,
                warnings,
            )?;
            nums.push(value.to_pdf(doc, warnings)?, warnings)?;
        }

        let mut res = qpdf::objects::Dict::new(doc, warnings)?;
        res.set(
            qpdf::keys::NUMS,
            nums.clone().make_indirect(warnings)?,
            warnings,
        )?;
        Ok(res)
    }
}

impl serde::Serialize for PageLabels {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        use serde::ser::SerializeMap;
        let mut map = serializer.serialize_map(Some(self.0.len()))?;
        for (k, v) in self.0.iter() {
            let k = (k + 1).to_string();
            map.serialize_entry(&k, v)?;
        }

        map.end()
    }
}

impl<'de> serde::Deserialize<'de> for PageLabels {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        use serde::de::Error;
        use serde::de::Visitor;

        struct MapVisitor;

        impl<'de> Visitor<'de> for MapVisitor {
            type Value = PageLabels;

            fn expecting(
                &self,
                formatter: &mut std::fmt::Formatter,
            ) -> std::fmt::Result {
                formatter.write_str("a page label map")
            }

            fn visit_map<M>(self, mut map: M) -> Result<Self::Value, M::Error>
            where
                M: serde::de::MapAccess<'de>,
            {
                let mut res = PageLabelMap::new();

                while let Some((key, value)) =
                    map.next_entry::<&str, LabelFormat>()?
                {
                    let key: u32 = key.parse().map_err(|_| {
                        M::Error::invalid_value(
                            serde::de::Unexpected::Str(key),
                            &"an integer greater than 0",
                        )
                    })?;
                    if key == 0 {
                        return Err(M::Error::invalid_value(
                            serde::de::Unexpected::Unsigned(key.into()),
                            &"an integer greater than 0",
                        ));
                    }
                    res.insert(key - 1, value);
                }

                Ok(PageLabels(res))
            }
        }

        deserializer.deserialize_map(MapVisitor)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn style_to_name() {
        let mut warnings = vec![];
        let mut doc = qpdf::Document::open(
            "testdata/PDF test document 1.pdf",
            &mut warnings,
        )
        .expect("Could not open document");

        let decimal = Style::Decimal
            .to_pdf(&mut doc, &mut warnings)
            .expect("Could not convert to PDF");
        assert_eq!(&decimal, DECIMAL);
        let uppercase_roman = Style::UppercaseRoman
            .to_pdf(&mut doc, &mut warnings)
            .expect("Could not convert to PDF");
        assert_eq!(&uppercase_roman, UPPERCASE_ROMAN);
        let lowercase_roman = Style::LowercaseRoman
            .to_pdf(&mut doc, &mut warnings)
            .expect("Could not convert to PDF");
        assert_eq!(&lowercase_roman, LOWERCASE_ROMAN);
        let uppercase_letters = Style::UppercaseLetters
            .to_pdf(&mut doc, &mut warnings)
            .expect("Could not convert to PDF");
        assert_eq!(&uppercase_letters, UPPERCASE_LETTERS);
        let lowercase_letters = Style::LowercaseLetters
            .to_pdf(&mut doc, &mut warnings)
            .expect("Could not convert to PDF");
        assert_eq!(&lowercase_letters, LOWERCASE_LETTERS);

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn name_to_style() {
        let mut warnings = vec![];
        let mut doc = qpdf::Document::open(
            "testdata/PDF test document 1.pdf",
            &mut warnings,
        )
        .expect("Could not open document");

        assert_eq!(
            Style::from_pdf(
                &qpdf::objects::Name::new(DECIMAL, &mut doc, &mut warnings)
                    .expect("Could not create Name"),
                &mut warnings,
            )
            .expect("Could not decode style"),
            Style::Decimal,
        );
        assert_eq!(
            Style::from_pdf(
                &qpdf::objects::Name::new(
                    UPPERCASE_ROMAN,
                    &mut doc,
                    &mut warnings
                )
                .expect("Could not create Name"),
                &mut warnings,
            )
            .expect("Could not decode style"),
            Style::UppercaseRoman,
        );
        assert_eq!(
            Style::from_pdf(
                &qpdf::objects::Name::new(
                    LOWERCASE_ROMAN,
                    &mut doc,
                    &mut warnings
                )
                .expect("Could not create Name"),
                &mut warnings,
            )
            .expect("Could not decode style"),
            Style::LowercaseRoman,
        );
        assert_eq!(
            Style::from_pdf(
                &qpdf::objects::Name::new(
                    UPPERCASE_LETTERS,
                    &mut doc,
                    &mut warnings
                )
                .expect("Could not create Name"),
                &mut warnings,
            )
            .expect("Could not decode style"),
            Style::UppercaseLetters,
        );
        assert_eq!(
            Style::from_pdf(
                &qpdf::objects::Name::new(
                    LOWERCASE_LETTERS,
                    &mut doc,
                    &mut warnings
                )
                .expect("Could not create Name"),
                &mut warnings,
            )
            .expect("Could not decode style"),
            Style::LowercaseLetters,
        );

        let error = Style::from_pdf(
            &qpdf::objects::Name::new("blorp", &mut doc, &mut warnings)
                .expect("Could not create Name"),
            &mut warnings,
        )
        .expect_err("Created a bogus Style??");
        assert_eq!(error.kind, qpdf::error::ErrorKind::Object);

        assert_eq!(warnings.len(), 0);
    }

    fn make_rust_page_labels() -> PageLabels {
        let mut res = PageLabelMap::new();
        // OK, an empty conversion worked, now let's try adding some actual
        // content.
        res.insert(
            0,
            LabelFormat {
                prefix: Some("Cover".into()),
                ..Default::default()
            },
        );
        res.insert(
            1,
            LabelFormat {
                style: Some(Style::LowercaseRoman),
                start: Some(unsafe { NonZeroU32::new_unchecked(1) }),
                ..Default::default()
            },
        );
        res.insert(
            5,
            LabelFormat {
                style: Some(Style::Decimal),
                start: Some(unsafe { NonZeroU32::new_unchecked(1) }),
                ..Default::default()
            },
        );

        PageLabels(res)
    }

    fn assert_round_trip_from_rust(
        page_labels: &PageLabels,
        doc: &mut qpdf::Document,
        warnings: &mut qpdf::Warnings,
    ) {
        let pdf_page_labels = page_labels
            .to_pdf(doc, warnings)
            .expect("Could not convert empty PageLabels to PDF");
        let rust_page_labels = PageLabels::from_pdf(&pdf_page_labels, warnings)
            .expect("Could not convert PDF to PageLabels");
        assert_eq!(page_labels, &rust_page_labels);
    }

    #[test]
    fn round_trip_from_rust_empty() {
        let mut warnings = vec![];
        let mut doc = qpdf::Document::open(
            "testdata/PDF test document 1.pdf",
            &mut warnings,
        )
        .expect("Could not open document");

        let page_labels = PageLabels(PageLabelMap::new());

        assert_round_trip_from_rust(&page_labels, &mut doc, &mut warnings);

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn round_trip_from_rust_non_empty() {
        let mut warnings = vec![];
        let mut doc = qpdf::Document::open(
            "testdata/PDF test document 1.pdf",
            &mut warnings,
        )
        .expect("Could not open document");

        let page_labels = make_rust_page_labels();

        assert_round_trip_from_rust(&page_labels, &mut doc, &mut warnings);

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn convert_depth_one_number_tree() {
        let mut warnings = vec![];
        let mut doc = qpdf::Document::open(
            "testdata/PDF test document 1.pdf",
            &mut warnings,
        )
        .expect("Could not open document");

        let mut nums = qpdf::objects::Array::new(&mut doc, &mut warnings)
            .expect("Couldn't create names array");

        nums.push(
            qpdf::objects::Integer::new(0, &mut doc, &mut warnings)
                .expect("Could not create integer"),
            &mut warnings,
        )
        .expect("Could not push index");
        nums.push(
            LabelFormat {
                prefix: Some("Cover".into()),
                ..Default::default()
            }
            .to_pdf(&mut doc, &mut warnings)
            .expect("Could not create label format")
            .make_indirect(&mut warnings)
            .expect("Could not make label format indirect"),
            &mut warnings,
        )
        .expect("Could not push index");

        nums.push(
            qpdf::objects::Integer::new(1, &mut doc, &mut warnings)
                .expect("Could not create integer"),
            &mut warnings,
        )
        .expect("Could not push index");
        nums.push(
            LabelFormat {
                style: Some(Style::LowercaseRoman),
                start: Some(unsafe { NonZeroU32::new_unchecked(1) }),
                ..Default::default()
            }
            .to_pdf(&mut doc, &mut warnings)
            .expect("Could not create label format")
            .make_indirect(&mut warnings)
            .expect("Could not make label format indirect"),
            &mut warnings,
        )
        .expect("Could not push index");

        nums.push(
            qpdf::objects::Integer::new(5, &mut doc, &mut warnings)
                .expect("Could not create integer"),
            &mut warnings,
        )
        .expect("Could not push index");
        nums.push(
            LabelFormat {
                style: Some(Style::Decimal),
                start: Some(unsafe { NonZeroU32::new_unchecked(1) }),
                ..Default::default()
            }
            .to_pdf(&mut doc, &mut warnings)
            .expect("Could not create label format")
            .make_indirect(&mut warnings)
            .expect("Could not make label format indirect"),
            &mut warnings,
        )
        .expect("Could not push index");

        let mut root = qpdf::objects::Dict::new(&mut doc, &mut warnings)
            .expect("Could not create dict");
        root.set(
            qpdf::keys::NUMS,
            nums.make_indirect(&mut warnings)
                .expect("Could not make nums array indirect"),
            &mut warnings,
        )
        .expect("Could not insert Nums key");

        let actual = PageLabels::from_pdf(&root, &mut warnings)
            .expect("Could not convert PDF PageLabels structure to Rust.");

        let expected = make_rust_page_labels();

        assert_eq!(actual, expected);
        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn convert_depth_two_number_tree() {
        let mut warnings = vec![];
        let mut doc = qpdf::Document::open(
            "testdata/PDF test document 1.pdf",
            &mut warnings,
        )
        .expect("Could not open document");

        let mut nums1 = qpdf::objects::Array::new(&mut doc, &mut warnings)
            .expect("Couldn't create names array");
        nums1
            .push(
                qpdf::objects::Integer::new(0, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push index");
        nums1
            .push(
                LabelFormat {
                    prefix: Some("Cover".into()),
                    ..Default::default()
                }
                .to_pdf(&mut doc, &mut warnings)
                .expect("Could not create label format")
                .make_indirect(&mut warnings)
                .expect("Could not make label format indirect"),
                &mut warnings,
            )
            .expect("Could not push index");

        nums1
            .push(
                qpdf::objects::Integer::new(1, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push index");
        nums1
            .push(
                LabelFormat {
                    style: Some(Style::LowercaseRoman),
                    start: Some(unsafe { NonZeroU32::new_unchecked(1) }),
                    ..Default::default()
                }
                .to_pdf(&mut doc, &mut warnings)
                .expect("Could not create label format")
                .make_indirect(&mut warnings)
                .expect("Could not make label format indirect"),
                &mut warnings,
            )
            .expect("Could not push index");

        let mut limits1 = qpdf::objects::Array::new(&mut doc, &mut warnings)
            .expect("Could not create limits array");
        limits1
            .push(
                qpdf::objects::Integer::new(0, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push lower limit");
        limits1
            .push(
                qpdf::objects::Integer::new(1, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push upper limit");

        let mut leaf1 = qpdf::objects::Dict::new(&mut doc, &mut warnings)
            .expect("Could not create dict");
        leaf1
            .set(
                qpdf::keys::NUMS,
                nums1
                    .make_indirect(&mut warnings)
                    .expect("Could not make nums array indirect"),
                &mut warnings,
            )
            .expect("Could not insert Nums key");
        leaf1
            .set(qpdf::keys::LIMITS, limits1, &mut warnings)
            .expect("Could not insert Limits key");

        let mut nums2 = qpdf::objects::Array::new(&mut doc, &mut warnings)
            .expect("Couldn't create names array");
        nums2
            .push(
                qpdf::objects::Integer::new(5, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push index");
        nums2
            .push(
                LabelFormat {
                    style: Some(Style::Decimal),
                    start: Some(unsafe { NonZeroU32::new_unchecked(1) }),
                    ..Default::default()
                }
                .to_pdf(&mut doc, &mut warnings)
                .expect("Could not create label format")
                .make_indirect(&mut warnings)
                .expect("Could not make label format indirect"),
                &mut warnings,
            )
            .expect("Could not push index");

        let mut limits2 = qpdf::objects::Array::new(&mut doc, &mut warnings)
            .expect("Could not create limits array");
        limits2
            .push(
                qpdf::objects::Integer::new(5, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push lower limit");
        limits2
            .push(
                qpdf::objects::Integer::new(5, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push upper limit");

        let mut leaf2 = qpdf::objects::Dict::new(&mut doc, &mut warnings)
            .expect("Could not create dict");
        leaf2
            .set(
                qpdf::keys::NUMS,
                nums2
                    .make_indirect(&mut warnings)
                    .expect("Could not make nums array indirect"),
                &mut warnings,
            )
            .expect("Could not insert Nums key");
        leaf2
            .set(qpdf::keys::LIMITS, limits2, &mut warnings)
            .expect("Could not insert Limits key");

        let mut root_kids = qpdf::objects::Array::new(&mut doc, &mut warnings)
            .expect("Could not create root kids array");
        root_kids
            .push(
                leaf1
                    .make_indirect(&mut warnings)
                    .expect("Could not make leaf1 indirect"),
                &mut warnings,
            )
            .expect("Could not push leaf1 reference");
        root_kids
            .push(
                leaf2
                    .make_indirect(&mut warnings)
                    .expect("Could not make leaf2 indirect"),
                &mut warnings,
            )
            .expect("Could not push leaf2 reference");

        let mut root = qpdf::objects::Dict::new(&mut doc, &mut warnings)
            .expect("Could not create root dict");
        root.set(qpdf::keys::KIDS, root_kids, &mut warnings)
            .expect("Could not set Kids key");

        let actual = PageLabels::from_pdf(&root, &mut warnings)
            .expect("Could not convert PDF PageLabels structure to Rust.");
        let expected = make_rust_page_labels();

        assert_eq!(actual, expected);
        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn convert_depth_three_number_tree() {
        let mut warnings = vec![];
        let mut doc = qpdf::Document::open(
            "testdata/PDF test document 1.pdf",
            &mut warnings,
        )
        .expect("Could not open document");

        let mut nums1 = qpdf::objects::Array::new(&mut doc, &mut warnings)
            .expect("Couldn't create names array");
        nums1
            .push(
                qpdf::objects::Integer::new(0, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push index");
        nums1
            .push(
                LabelFormat {
                    prefix: Some("Cover".into()),
                    ..Default::default()
                }
                .to_pdf(&mut doc, &mut warnings)
                .expect("Could not create label format")
                .make_indirect(&mut warnings)
                .expect("Could not make label format indirect"),
                &mut warnings,
            )
            .expect("Could not push index");

        let mut limits1 = qpdf::objects::Array::new(&mut doc, &mut warnings)
            .expect("Could not create limits array");
        limits1
            .push(
                qpdf::objects::Integer::new(0, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push lower limit");
        limits1
            .push(
                qpdf::objects::Integer::new(0, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push upper limit");

        let mut leaf1 = qpdf::objects::Dict::new(&mut doc, &mut warnings)
            .expect("Could not create dict");
        leaf1
            .set(
                qpdf::keys::NUMS,
                nums1
                    .make_indirect(&mut warnings)
                    .expect("Could not make nums array indirect"),
                &mut warnings,
            )
            .expect("Could not insert Nums key");
        leaf1
            .set(qpdf::keys::LIMITS, limits1, &mut warnings)
            .expect("Could not insert Limits key");

        let mut nums2 = qpdf::objects::Array::new(&mut doc, &mut warnings)
            .expect("Couldn't create names array");
        nums2
            .push(
                qpdf::objects::Integer::new(1, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push index");
        nums2
            .push(
                LabelFormat {
                    style: Some(Style::LowercaseRoman),
                    start: Some(unsafe { NonZeroU32::new_unchecked(1) }),
                    ..Default::default()
                }
                .to_pdf(&mut doc, &mut warnings)
                .expect("Could not create label format")
                .make_indirect(&mut warnings)
                .expect("Could not make label format indirect"),
                &mut warnings,
            )
            .expect("Could not push index");

        let mut limits2 = qpdf::objects::Array::new(&mut doc, &mut warnings)
            .expect("Could not create limits array");
        limits2
            .push(
                qpdf::objects::Integer::new(1, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push lower limit");
        limits2
            .push(
                qpdf::objects::Integer::new(1, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push upper limit");

        let mut leaf2 = qpdf::objects::Dict::new(&mut doc, &mut warnings)
            .expect("Could not create dict");
        leaf2
            .set(
                qpdf::keys::NUMS,
                nums2
                    .make_indirect(&mut warnings)
                    .expect("Could not make nums array indirect"),
                &mut warnings,
            )
            .expect("Could not insert Nums key");
        leaf2
            .set(qpdf::keys::LIMITS, limits2, &mut warnings)
            .expect("Could not insert Limits key");

        let mut intermediate_kids =
            qpdf::objects::Array::new(&mut doc, &mut warnings)
                .expect("Could not create intermediate kids array");
        intermediate_kids
            .push(
                leaf1
                    .make_indirect(&mut warnings)
                    .expect("Could not make leaf1 indirect"),
                &mut warnings,
            )
            .expect("Could not push leaf1 reference to intermediate_kids");
        intermediate_kids
            .push(
                leaf2
                    .make_indirect(&mut warnings)
                    .expect("Could not make leaf2 indirect"),
                &mut warnings,
            )
            .expect("Could not push leaf2 reference to intermediate_kids");

        let mut intermediate_limits =
            qpdf::objects::Array::new(&mut doc, &mut warnings)
                .expect("Could not create intermediate_limits array");
        intermediate_limits
            .push(
                qpdf::objects::Integer::new(0, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push lower limit");
        intermediate_limits
            .push(
                qpdf::objects::Integer::new(1, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push lower limit");

        let mut intermediate =
            qpdf::objects::Dict::new(&mut doc, &mut warnings)
                .expect("Could not create intermeditate node");
        intermediate
            .set(qpdf::keys::KIDS, intermediate_kids, &mut warnings)
            .expect("Could not insert Kids key to intermediate node");
        intermediate
            .set(qpdf::keys::LIMITS, intermediate_limits, &mut warnings)
            .expect("Could not insert Limits key to intermediate node");

        let mut nums3 = qpdf::objects::Array::new(&mut doc, &mut warnings)
            .expect("Couldn't create names array");
        nums3
            .push(
                qpdf::objects::Integer::new(5, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push index");
        nums3
            .push(
                LabelFormat {
                    style: Some(Style::Decimal),
                    start: Some(unsafe { NonZeroU32::new_unchecked(1) }),
                    ..Default::default()
                }
                .to_pdf(&mut doc, &mut warnings)
                .expect("Could not create label format")
                .make_indirect(&mut warnings)
                .expect("Could not make label format indirect"),
                &mut warnings,
            )
            .expect("Could not push index");

        let mut limits3 = qpdf::objects::Array::new(&mut doc, &mut warnings)
            .expect("Could not create limits array");
        limits3
            .push(
                qpdf::objects::Integer::new(5, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push lower limit");
        limits3
            .push(
                qpdf::objects::Integer::new(5, &mut doc, &mut warnings)
                    .expect("Could not create integer"),
                &mut warnings,
            )
            .expect("Could not push upper limit");

        let mut leaf3 = qpdf::objects::Dict::new(&mut doc, &mut warnings)
            .expect("Could not create dict");
        leaf3
            .set(
                qpdf::keys::NUMS,
                nums3
                    .make_indirect(&mut warnings)
                    .expect("Could not make nums array indirect"),
                &mut warnings,
            )
            .expect("Could not insert Nums key");
        leaf3
            .set(qpdf::keys::LIMITS, limits3, &mut warnings)
            .expect("Could not insert Limits key");

        let mut root_kids = qpdf::objects::Array::new(&mut doc, &mut warnings)
            .expect("Could not create root kids array");
        root_kids
            .push(
                intermediate
                    .make_indirect(&mut warnings)
                    .expect("Could not make leaf1 indirect"),
                &mut warnings,
            )
            .expect("Could not push leaf1 reference");
        root_kids
            .push(
                leaf3
                    .make_indirect(&mut warnings)
                    .expect("Could not make leaf2 indirect"),
                &mut warnings,
            )
            .expect("Could not push leaf2 reference");

        let mut root = qpdf::objects::Dict::new(&mut doc, &mut warnings)
            .expect("Could not create root dict");
        root.set(qpdf::keys::KIDS, root_kids, &mut warnings)
            .expect("Could not set Kids key");

        let actual = PageLabels::from_pdf(&root, &mut warnings)
            .expect("Could not convert PDF PageLabels structure to Rust.");
        let expected = make_rust_page_labels();

        assert_eq!(actual, expected);
        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn deserialize_toml() {
        let actual: PageLabels = toml::from_str(
            r#"
            [1]
            prefix = "Cover"

            [2]
            style = "LowercaseRoman"
            start = 1

            [6]
            style = "Decimal"
            start = 1
            "#,
        )
        .expect("Could not parse TOML");

        let expected = make_rust_page_labels();

        assert_eq!(actual, expected);
    }

    #[test]
    fn reject_zero_page_index() {
        let maybe_labels: Result<PageLabels, _> =
            toml::from_str(r#"0 = { prefix = "blorp" }"#);

        let err =
            maybe_labels.expect_err("Parsed toml with invalid page index");

        assert_eq!(
            err.to_string(),
            "invalid value: integer `0`, expected an integer greater than 0 at line 1 column 1",
        );
    }
}
