use serde::{Deserialize, Serialize};

#[derive(
    Default, Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize,
)]
pub struct Metadata {
    pub author: Option<String>,
    pub title: Option<String>,
    pub producer: Option<String>,
    pub creator: Option<String>,
    pub creation_date: Option<String>,
    pub modification_date: Option<String>,
    pub subject: Option<String>,
    pub keywords: Option<String>,
}

// NOTE: Metadata doesn't implement FromPdf or ToPdf
// because although it looks very much like the schema of
// PDF's Document Information Directory (section 14.3.3),
// it's actually different:
// it models the patching instructions from the patch file,
// not the data in the PDF.

impl Metadata {
    pub fn from_document(
        doc: &qpdf::Document,
        warnings: &mut qpdf::Warnings,
    ) -> qpdf::Result<Metadata> {
        let trailer: qpdf::objects::Dict = doc.trailer(warnings)?.try_into()?;
        let maybe_info: Option<qpdf::objects::Dict> =
            trailer.get(qpdf::keys::INFO, warnings)?.into();

        let mut get_key = |key| -> Option<String> {
            maybe_info
                .clone()
                .and_then(|info| info.get(key, warnings).ok())
                .and_then(Into::into)
        };

        Ok(Metadata {
            author: get_key(qpdf::keys::AUTHOR),
            title: get_key(qpdf::keys::TITLE),
            producer: get_key(qpdf::keys::PRODUCER),
            creator: get_key(qpdf::keys::CREATOR),
            creation_date: get_key(qpdf::keys::CREATION_DATE),
            modification_date: get_key(qpdf::keys::MOD_DATE),
            subject: get_key(qpdf::keys::SUBJECT),
            keywords: get_key(qpdf::keys::KEYWORDS),
        })
    }

    pub fn inject_into(
        &self,
        doc: &mut qpdf::Document,
        warnings: &mut qpdf::Warnings,
    ) -> qpdf::Result<()> {
        let mut trailer: qpdf::objects::Dict =
            doc.trailer(warnings)?.try_into()?;
        let maybe_info: Option<qpdf::objects::Dict> =
            trailer.get(qpdf::keys::INFO, warnings)?.into();
        let mut info = if let Some(i) = maybe_info {
            // If the trailer already has an Info dict, use it.
            i
        } else {
            // Otherwise, let's make an Info dict of our own...
            let info = qpdf::objects::Dict::new(doc, warnings)?;

            // ...stash an indirect pointer in the trailer...
            trailer.set(
                qpdf::keys::INFO,
                info.clone().make_indirect(warnings)?,
                warnings,
            )?;

            // ...and use that.
            info
        };

        let mut set_key = |key, value: &Option<String>| {
            // If we need to modify this key in the dict...
            if let Some(s) = value {
                info.set(
                    key,
                    if s == "" {
                        // Delete this key
                        qpdf::objects::Value::from(qpdf::objects::Null::new(
                            doc, warnings,
                        )?)
                    } else {
                        // Set a value
                        qpdf::objects::Value::from(
                            qpdf::objects::String::from_text(s, doc, warnings)?,
                        )
                    },
                    warnings,
                )
            // Otherwise, nothing to do.
            } else {
                Ok(())
            }
        };

        set_key(qpdf::keys::AUTHOR, &self.author)?;
        set_key(qpdf::keys::AUTHOR, &self.author)?;
        set_key(qpdf::keys::TITLE, &self.title)?;
        set_key(qpdf::keys::PRODUCER, &self.producer)?;
        set_key(qpdf::keys::CREATOR, &self.creator)?;
        set_key(qpdf::keys::CREATION_DATE, &self.creation_date)?;
        set_key(qpdf::keys::MOD_DATE, &self.modification_date)?;
        set_key(qpdf::keys::SUBJECT, &self.subject)?;
        set_key(qpdf::keys::KEYWORDS, &self.keywords)?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn inject_metadata() {
        let mut warnings = vec![];
        let mut doc = qpdf::Document::open(
            "testdata/PDF test document 1.pdf",
            &mut warnings,
        )
        .expect("Couldn't open source");

        let mut metadata = Metadata::from_document(&doc, &mut warnings)
            .expect("Could not read metadata from document");

        // Update the title
        metadata.title = Some("A new title".into());

        // Ignore the keywords
        metadata.keywords = None;

        // Delete the creator field
        metadata.creator = Some("".into());

        // Apply the new metadata to the document.
        metadata
            .inject_into(&mut doc, &mut warnings)
            .expect("Could not inject metadata");

        // Read out the new metadata to make sure it's modified.
        let new_metadata = Metadata::from_document(&doc, &mut warnings)
            .expect("Could not read metadata a second time");

        // The title should be updated
        assert_eq!(new_metadata.title.unwrap(), "A new title");
        // The keywords should not be changed
        assert_eq!(new_metadata.keywords.unwrap(), "PDF, testing, metadata");
        // The creator field should be deleted.
        assert_eq!(new_metadata.creator, None);

        assert_eq!(warnings.len(), 0);
    }
}
