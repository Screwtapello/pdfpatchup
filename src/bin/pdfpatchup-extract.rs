use std::env;
use std::fs;

use pdfpatchup;
use qpdf;
use toml;

fn main() {
    let mut args = env::args().skip(1);
    let source = args.next().unwrap();
    let target = args.next().unwrap();
    let mut warnings = vec![];

    let document = qpdf::Document::open(&source, &mut warnings).unwrap();
    let patch =
        pdfpatchup::Patch::from_document(&document, &mut warnings).unwrap();
    let output = toml::to_string(&patch).unwrap();

    fs::write(target, output).unwrap();
}
