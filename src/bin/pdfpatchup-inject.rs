use std::env;
use std::fs;

use pdfpatchup;
use qpdf;
use toml;

fn main() {
    let mut args = env::args().skip(1);
    let source = args.next().unwrap();
    let patch_file = args.next().unwrap();
    let target_file = args.next().unwrap();
    let mut warnings = vec![];

    let mut document = qpdf::Document::open(&source, &mut warnings)
        .expect("Could not read source document");

    let patch_toml =
        fs::read_to_string(patch_file).expect("Could not read patch file");
    let patch: pdfpatchup::Patch =
        toml::from_str(&patch_toml).expect("Could not parse patch file");

    patch
        .inject_into(&mut document, &mut warnings)
        .expect("Could not inject metacontent");

    document
        .write(&target_file, &mut warnings)
        .expect("Could not write PDF to target file");

    for each in warnings.into_iter() {
        eprintln!("{}", each);
    }
}
