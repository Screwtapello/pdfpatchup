Types of metacontent
======================

  - outlines
  - annotations

Enhancements
============

  - The `Recogniser` should include the PDF trailer's `/ID` value
    as one of the document features it recognises
      - I don't know how frequently it lines up with the semantics we want
        (A unique identifier for a document despite revisions)
        but that's what it's *supposed* to be for,
        so I'm hopeful
  - Handle XMP metadata, not just the `/Info` dict
      - But then we'd need XML and RDF parsers..!
  - Add the .unparse() value to ObjectHandle's Debug format.

API changes
===========

  - Add some kind of `DocumentHandle::with_borrowed_mut()` API
    so we can automate lifetime management of mut-borrowed DocumentHandles
    without risking borrowing them twice and causing crashes.
      - We can also use this to automate calling check_error().
  - Make things take `AsMutRef<DocumentHandle>` rather than `&mut Document`,
    and then make objects implement `AsMutRef<DocumentHandle>` too.
    That way, a class can construct a new object in the same document
    as an existing object, without having to pass the document around too.
  - Remove the `qpdf::objects::ArrayIterator` struct,
    because the array object is reference-counted
    and we can't be sure no other handles to it exist
    when we start iterating over it.

Documentation
=============
