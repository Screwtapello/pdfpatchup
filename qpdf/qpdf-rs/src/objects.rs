//! Types representing the PDF object model
//!
//! Note that these are *handles*
//! to the actual (reference-counted) objects
//! stored inside the PDF data.
//! Although they can be cloned for memory-management purposes,
//! cloning a container gives you two handles to the same container,
//! not two containers with the same contents.
//!
//! Where possible,
//! these handles can be converted into Rust native values
//! through the [`TryInto`] trait:
//!
//! ```
//! # fn dummy() -> qpdf::Result<()> {
//! use qpdf::objects;
//!
//! // We always need a place to stash warnings.
//! let mut warnings = vec![];
//! # let mut document = qpdf::Document::open(
//! #     "../../testdata/PDF test document 1.pdf",
//! #     &mut warnings,
//! # )?;
//!
//! // Create a handle to a boolean PDF object containing `true`.
//! let bool_handle = objects::Bool::new(true, &mut document, &mut warnings)?;
//!
//! // Extract the value from the handle
//! let extracted: bool = (&bool_handle).try_into()?;
//!
//! assert_eq!(extracted, true);
//! assert_eq!(warnings.len(), 0);
//! # Ok(()) }
//! # dummy().unwrap();
//! ```

use std::ffi;
use std::ops::Deref;
use std::result;

use crate::error;
use crate::error::Result;
use crate::Document;
use crate::Warnings;

// XXX: Is there a way to compare objects by identity, since different handles
// can refer to the same object?

/// A Rust wrapper around QPDF's PDF object handle type
///
/// This type wraps the QPDF's object handle type,
/// using Rust's type safety to manage
/// cloning and dropping handles when needed.
///
/// This type is used inside all the other higher-level type in this module,
/// like [`Integer`] or [`Dict`] or [`Value`].
#[derive(Debug)]
pub struct RawObject {
    doc_handle: Document,
    obj_handle: qpdf_sys::qpdf_oh,
}

impl RawObject {
    pub(crate) fn new(
        doc_handle: Document,
        obj_handle: qpdf_sys::qpdf_oh,
    ) -> RawObject {
        RawObject {
            doc_handle,
            obj_handle,
        }
    }

    /// Create [`Indirect`] object that points to this one.
    pub fn make_indirect(&self, warnings: &mut Warnings) -> Result<Indirect> {
        let new_handle =
            self.doc_handle
                .call_with_warnings(warnings, |raw_doc| unsafe {
                    qpdf_sys::qpdf_make_indirect_object(
                        raw_doc,
                        self.obj_handle,
                    )
                })?;
        Ok(Indirect(RawObject::new(
            self.doc_handle.clone(),
            new_handle,
        )))
    }

    // This function doesn't take warnings because we need to call it from
    // inside a TryFrom implementation, where we don't have access to a warnings
    // vec.
    fn get_type_code(&self) -> Result<qpdf_sys::qpdf_object_type_e> {
        self.doc_handle.call_without_warnings(|raw_doc| unsafe {
            qpdf_sys::qpdf_oh_get_type_code(raw_doc, self.obj_handle)
        })
    }

    fn is_indirect(&self) -> Result<bool> {
        let res = self.doc_handle.call_without_warnings(|raw_doc| unsafe {
            qpdf_sys::qpdf_oh_is_indirect(raw_doc, self.obj_handle)
        })?;
        Ok(res == qpdf_sys::QPDF_TRUE)
    }

    /// Serialise this object using PDF object notation
    ///
    /// ```
    /// # fn dummy() -> qpdf::Result<()> {
    /// use qpdf::objects;
    ///
    /// // We always need a place to stash warnings.
    /// let mut warnings = vec![];
    /// # let mut document = qpdf::Document::open(
    /// #     "../../testdata/PDF test document 1.pdf",
    /// #     &mut warnings,
    /// # )?;
    ///
    /// // Create an object representing an integer
    /// let integer = objects::Integer::new(42, &mut document, &mut warnings)?;
    ///
    /// // Serialise this object using PDF object notation
    /// let literal = integer.unparse(&mut warnings)?;
    ///
    /// // PDF object notation writes an integer as "42",
    /// // just like every other language
    /// assert_eq!(literal, b"42");
    /// assert_eq!(warnings.len(), 0);
    /// # Ok(()) }
    /// # dummy().unwrap();
    /// ```
    pub fn unparse(&self, warnings: &mut Warnings) -> Result<Vec<u8>> {
        let ptr =
            self.doc_handle
                .call_with_warnings(warnings, |raw_doc| unsafe {
                    qpdf_sys::qpdf_oh_unparse(raw_doc, self.obj_handle)
                        as *const u8 // Rust wants chars as u8, not i8
                })?;
        let length = self
            .doc_handle
            .call_with_warnings(warnings, |raw_doc| unsafe {
                qpdf_sys::qpdf_get_last_string_length(raw_doc)
            })?;

        let byteslice =
            unsafe { std::slice::from_raw_parts(ptr, length as usize) };
        Ok(byteslice.to_owned())
    }
}

impl Drop for RawObject {
    fn drop(&mut self) {
        self.doc_handle
            .call_without_warnings(|raw_doc| unsafe {
                qpdf_sys::qpdf_oh_release(raw_doc, self.obj_handle)
            })
            .expect("Could not drop object handle")
    }
}

impl Clone for RawObject {
    fn clone(&self) -> Self {
        let new_handle = self
            .doc_handle
            .call_without_warnings(|raw_doc| unsafe {
                qpdf_sys::qpdf_oh_new_object(raw_doc, self.obj_handle)
            })
            .expect("Could not clone object handle");

        RawObject {
            doc_handle: self.doc_handle.clone(),
            obj_handle: new_handle,
        }
    }
}

impl Deref for RawObject {
    type Target = qpdf_sys::qpdf_oh;

    fn deref(&self) -> &Self::Target {
        &self.obj_handle
    }
}

/// A sentinel for an uninitialized object
#[derive(Debug, Clone)]
pub struct Uninitialized(RawObject);

/// A sentinel object
#[derive(Debug, Clone)]
pub struct Null(RawObject);

impl Null {
    pub fn new(doc: &mut Document, warnings: &mut Warnings) -> Result<Null> {
        let obj_handle = doc
            .call_with_warnings(warnings, |raw_doc| unsafe {
                qpdf_sys::qpdf_oh_new_null(raw_doc)
            })?;

        Ok(Null(RawObject::new(doc.clone(), obj_handle)))
    }
}

/// A PDF object that can be true or false
#[derive(Debug, Clone)]
pub struct Bool(RawObject);

impl Bool {
    pub fn new(
        value: bool,
        doc: &mut Document,
        warnings: &mut Warnings,
    ) -> Result<Bool> {
        let obj_handle =
            doc.call_with_warnings(warnings, |raw_doc| unsafe {
                qpdf_sys::qpdf_oh_new_bool(
                    raw_doc,
                    if value {
                        qpdf_sys::QPDF_TRUE
                    } else {
                        qpdf_sys::QPDF_FALSE
                    },
                )
            })?;

        Ok(Bool(RawObject::new(doc.clone(), obj_handle)))
    }
}

// XXX: If we explicitly create a bool object,
// or inspect an object handle and determine it's a bool,
// how dangerous would it be to blindly assume it's a bool later?
// We could use From<Bool> instead of TryFrom<Bool>,
// which would make things more convenient.
impl TryFrom<&Bool> for bool {
    type Error = error::Error;

    fn try_from(value: &Bool) -> result::Result<Self, Self::Error> {
        let result =
            value.doc_handle.call_without_warnings(|raw_doc| unsafe {
                qpdf_sys::qpdf_oh_get_bool_value(raw_doc, value.obj_handle)
            })?;
        Ok(result == qpdf_sys::QPDF_TRUE)
    }
}

/// A PDF object representing an integer number
#[derive(Debug, Clone)]
pub struct Integer(RawObject);

impl Integer {
    pub fn new(
        value: i64,
        doc: &mut Document,
        warnings: &mut Warnings,
    ) -> Result<Integer> {
        let obj_handle = doc
            .call_with_warnings(warnings, |raw_doc| unsafe {
                qpdf_sys::qpdf_oh_new_integer(raw_doc, value)
            })?;

        Ok(Integer(RawObject::new(doc.clone(), obj_handle)))
    }
}

impl TryFrom<&Integer> for i64 {
    type Error = error::Error;

    fn try_from(value: &Integer) -> result::Result<Self, Self::Error> {
        value.doc_handle.call_without_warnings(|raw_doc| unsafe {
            qpdf_sys::qpdf_oh_get_int_value(raw_doc, value.obj_handle)
        })
    }
}

/// A PDF object representing a real number
#[derive(Debug, Clone)]
pub struct Real(RawObject);

impl Real {
    pub fn new(
        value: f64,
        doc: &mut Document,
        warnings: &mut Warnings,
    ) -> Result<Real> {
        // FIXME: Apparently PDF reals aren't IEEE 754 binary64 floats,
        // they're implementation defined, so we need to treat them as strings
        // rather than as f64s.
        let obj_handle =
            doc.call_with_warnings(warnings, |raw_doc| unsafe {
                qpdf_sys::qpdf_oh_new_real_from_double(
                    raw_doc, value,
                    // An f64 has about 16 digits of precision.
                    16,
                )
            })?;

        Ok(Real(RawObject::new(doc.clone(), obj_handle)))
    }
}

impl TryFrom<&Real> for f64 {
    type Error = error::Error;

    fn try_from(value: &Real) -> result::Result<Self, Self::Error> {
        value.doc_handle.call_without_warnings(|raw_doc| unsafe {
            qpdf_sys::qpdf_oh_get_numeric_value(raw_doc, value.obj_handle)
        })
    }
}

/// Data ready to be used in creating a Name object.
///
/// Many QPDF APIs need a Name
/// (for example, [`Dict::get()`])
/// but do not take an actual [`Name`] object handle,
/// they take a pre-parsed copy of the Name data
/// that can be efficiently turned into a Name object if necessary.
/// This struct represents data in that format.
///
/// Because most methods that need Name data take a generic
/// `T: TryInto<NameData>` value,
/// and because that trait is implemented
/// for common types like `String` and `&str`,
/// you probably won't need to use this type directly.
/// Use a regular Rust string literal,
/// and it should Just Work.
///
/// ```
/// # fn dummy() -> qpdf::Result<()> {
/// use qpdf::objects;
///
/// // We always need a place to stash warnings.
/// let mut warnings = vec![];
/// # let mut document = qpdf::Document::open(
/// #     "../../testdata/PDF test document 1.pdf",
/// #     &mut warnings,
/// # )?;
///
/// // Create a value to store in a dict.
/// let dict_value = objects::Bool::new(true, &mut document, &mut warnings)?;
/// // Create a dict to store things in.
/// let mut dict = objects::Dict::new(&mut document, &mut warnings)?;
///
/// // Store the value in the dictionary under a key:
/// dict.set("SomeOption", dict_value, &mut warnings)?;
///
/// // No problems!
/// assert_eq!(warnings.len(), 0);
/// # Ok(()) }
/// # dummy().unwrap();
/// ```
#[derive(Debug, Clone, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct NameData {
    data: ffi::CString,
}

impl NameData {
    fn as_ptr(&self) -> *const std::os::raw::c_char {
        self.data.as_ptr()
    }
}

impl From<&ffi::CStr> for NameData {
    fn from(value: &ffi::CStr) -> Self {
        // value already has suitable content,
        // it just needs a leading `/`.
        let mut new_bytes = vec![b'/'];
        new_bytes.extend(value.to_bytes());
        Self {
            data: ffi::CString::new(new_bytes)
                .expect("CStr value had a NUL???"),
        }
    }
}

impl TryFrom<&[u8]> for NameData {
    type Error = error::Error;

    fn try_from(value: &[u8]) -> result::Result<Self, Self::Error> {
        let mut new_bytes = vec![b'/'];
        new_bytes.extend(value);
        Ok(Self {
            data: ffi::CString::new(new_bytes).map_err(|e| error::Error {
                filename: Default::default(),
                position: 0,
                kind: error::ErrorKind::Object,
                message: format!("Could not convert bytes to NameData: {}", e),
            })?,
        })
    }
}

impl TryFrom<&Name> for NameData {
    type Error = error::Error;

    fn try_from(value: &Name) -> result::Result<Self, Self::Error> {
        let ptr = value.doc_handle.call_without_warnings(|raw_doc| unsafe {
            qpdf_sys::qpdf_oh_get_name(raw_doc, value.obj_handle)
        })?;

        // Since we can only create Names via `char *`,
        // we assume names we pull out of a PDF cannot contain NUL bytes
        // either, so we don't need to care about getting the length.
        let value = unsafe { ffi::CStr::from_ptr(ptr) };
        Ok(value[1..].into())
    }
}

impl TryFrom<&str> for NameData {
    type Error = error::Error;

    fn try_from(value: &str) -> result::Result<Self, Self::Error> {
        value.as_bytes().try_into()
    }
}

impl<'a> From<&'a NameData> for &'a ffi::CStr {
    fn from(value: &'a NameData) -> &'a ffi::CStr {
        &(*value.data)[1..]
    }
}

impl PartialEq<ffi::CStr> for NameData {
    fn eq(&self, other: &ffi::CStr) -> bool {
        let s: &ffi::CStr = self.into();
        s == other
    }
}

impl<'a> From<&'a NameData> for &'a [u8] {
    fn from(value: &'a NameData) -> &'a [u8] {
        &value.data.to_bytes()[1..]
    }
}

impl PartialEq<[u8]> for NameData {
    fn eq(&self, other: &[u8]) -> bool {
        let s: &[u8] = self.into();
        s == other
    }
}

impl<'a> TryFrom<&'a NameData> for &'a str {
    type Error = error::Error;

    fn try_from(value: &'a NameData) -> result::Result<&'a str, Self::Error> {
        std::str::from_utf8(value.into()).map_err(|e| error::Error {
            filename: Default::default(),
            position: 0,
            kind: error::ErrorKind::Object,
            message: format!("Could not convert NameData to str: {}", e),
        })
    }
}

impl PartialEq<str> for NameData {
    fn eq(&self, other: &str) -> bool {
        let maybe_s: Option<&str> = self.try_into().ok();
        maybe_s.map(|s| s == other).unwrap_or(false)
    }
}
impl PartialEq<&str> for NameData {
    fn eq(&self, other: &&str) -> bool {
        let maybe_s: Option<&str> = self.try_into().ok();
        maybe_s.map(|s| s == *other).unwrap_or(false)
    }
}

/// A PDF object representing a well-known name
#[derive(Debug, Clone)]
pub struct Name(RawObject);

impl Name {
    pub fn new<T: TryInto<NameData, Error = error::Error>>(
        value: T,
        doc: &mut Document,
        warnings: &mut Warnings,
    ) -> Result<Name> {
        let value = value.try_into()?;
        let obj_handle =
            doc.call_with_warnings(warnings, |raw_doc| unsafe {
                // XXX: QPDF copies this data into a buffer it owns, right?
                // Right?
                qpdf_sys::qpdf_oh_new_name(raw_doc, value.as_ptr())
            })?;

        Ok(Name(RawObject::new(doc.clone(), obj_handle)))
    }
}

impl PartialEq<ffi::CStr> for Name {
    fn eq(&self, other: &ffi::CStr) -> bool {
        let maybe_data: Option<NameData> = self.try_into().ok();
        maybe_data.map(|d| &d == other).unwrap_or(false)
    }
}
impl PartialEq<[u8]> for Name {
    fn eq(&self, other: &[u8]) -> bool {
        let maybe_data: Option<NameData> = self.try_into().ok();
        maybe_data.map(|d| &d == other).unwrap_or(false)
    }
}
impl PartialEq<str> for Name {
    fn eq(&self, other: &str) -> bool {
        let maybe_data: Option<NameData> = self.try_into().ok();
        maybe_data.map(|d| &d == other).unwrap_or(false)
    }
}

/// A PDF object representing a sequence of bytes
#[derive(Debug, Clone)]
pub struct String(RawObject);

impl String {
    /// Create a String of bytes
    pub fn from_bytes<T: AsRef<[u8]>>(
        value: T,
        doc: &mut Document,
        warnings: &mut Warnings,
    ) -> Result<String> {
        let value = value.as_ref();
        let obj_handle =
            doc.call_with_warnings(warnings, |raw_doc| unsafe {
                qpdf_sys::qpdf_oh_new_binary_string(
                    raw_doc,
                    value.as_ptr() as *const std::os::raw::c_char,
                    value.len() as qpdf_sys::size_t,
                )
            })?;

        Ok(String(RawObject::new(doc.clone(), obj_handle)))
    }

    /// Create a String containing encoded Unicode text
    ///
    /// The exact encoding used depends on the text contents.
    /// The PDFDocEncoding (ISO 32000:2008 Annex D.3,
    /// similar to but not exactly like ISO-8859-1)
    /// will be used if the text only contains
    /// codepoints available in that encoding,
    /// otherwise the text will be encoded in
    /// UTF-16BE with a leading byte-order marker
    /// (see ISO 32000:2008 section 7.9.2.2).
    pub fn from_text<T: AsRef<str>>(
        value: T,
        doc: &mut Document,
        warnings: &mut Warnings,
    ) -> Result<String> {
        // FIXME: As of QPDF 10.6,
        // we can use qpdf_oh_new_binary_unicode_string()
        // to create a Unicode string possibly containing NULs,
        // as Rust and PDF strings allow.
        // Then we won't need this dangerous .unwrap().
        let value = ffi::CString::new(value.as_ref()).unwrap();
        let obj_handle =
            doc.call_with_warnings(warnings, |raw_doc| unsafe {
                qpdf_sys::qpdf_oh_new_unicode_string(
                    raw_doc,
                    value.as_ptr() as *const std::os::raw::c_char,
                )
            })?;

        Ok(String(RawObject::new(doc.clone(), obj_handle)))
    }
}

impl TryFrom<&String> for Vec<u8> {
    type Error = error::Error;

    fn try_from(value: &String) -> result::Result<Self, Self::Error> {
        let mut length: qpdf_sys::size_t = 0;
        let ptr = value.doc_handle.call_without_warnings(|raw_doc| unsafe {
            qpdf_sys::qpdf_oh_get_binary_string_value(
                raw_doc,
                value.obj_handle,
                &mut length as *mut _,
            ) as *const u8 // Rust wants chars as u8, not i8
        })?;

        let value = unsafe { std::slice::from_raw_parts(ptr, length as usize) };
        Ok(value.to_owned())
    }
}

impl PartialEq<[u8]> for String {
    fn eq(&self, other: &[u8]) -> bool {
        let maybe_s: Option<Vec<u8>> = self.try_into().ok();
        maybe_s.map(|s| s == other).unwrap_or(false)
    }
}

impl TryFrom<&String> for std::string::String {
    type Error = error::Error;

    fn try_from(value: &String) -> result::Result<Self, Self::Error> {
        let ptr = value.doc_handle.call_without_warnings(|raw_doc| unsafe {
            qpdf_sys::qpdf_oh_get_utf8_value(raw_doc, value.obj_handle)
                as *const u8 // Rust wants chars as u8, not i8
        })?;
        let length =
            value.doc_handle.call_without_warnings(|raw_doc| unsafe {
                qpdf_sys::qpdf_get_last_string_length(raw_doc)
            })?;

        let byteslice =
            unsafe { std::slice::from_raw_parts(ptr, length as usize) };
        // QPDF should automatically decode the string from UTF16-BE or Latin1
        // to UTF-8, it shouldn't be possible to get invalid UTF-8 out of it.
        std::str::from_utf8(byteslice)
            .map(|s| s.to_owned())
            .map_err(|_| error::Error {
                kind: error::ErrorKind::Object,
                filename: Default::default(),
                position: Default::default(),
                message: format!("Got malformed UTF-8: {:?}", byteslice),
            })
    }
}

impl PartialEq<str> for String {
    fn eq(&self, other: &str) -> bool {
        let maybe_s: Option<std::string::String> = self.try_into().ok();
        maybe_s.map(|s| s == other).unwrap_or(false)
    }
}

/// A PDF object representing an array of objects
///
/// Unfortunately, this cannot implement
/// the [`std::ops::Index`] or [`std::ops::IndexMut`] traits
/// because they require returning a reference to a Rust type,
/// while we have to synthesise a fresh object handle.
///
/// Note that if you create a data cycle
/// (such as by pushing an array into itself),
/// you're likely to get stack overflow crashes.
#[derive(Debug, Clone)]
pub struct Array(RawObject);

impl Array {
    pub fn new(doc: &mut Document, warnings: &mut Warnings) -> Result<Array> {
        let obj_handle = doc
            .call_with_warnings(warnings, |raw_doc| unsafe {
                qpdf_sys::qpdf_oh_new_array(raw_doc)
            })?;

        Ok(Array(RawObject::new(doc.clone(), obj_handle)))
    }

    /// Returns the number of items in this array.
    pub fn len(&self, warnings: &mut Warnings) -> Result<usize> {
        let res =
            self.doc_handle
                .call_with_warnings(warnings, |raw_doc| unsafe {
                    qpdf_sys::qpdf_oh_get_array_n_items(
                        raw_doc,
                        self.obj_handle,
                    )
                })? as usize;

        Ok(res)
    }

    /// Returns the item at a particular index in the array.
    pub fn get(&self, index: usize, warnings: &mut Warnings) -> Result<Value> {
        let item_handle =
            self.doc_handle
                .call_with_warnings(warnings, |raw_doc| unsafe {
                    qpdf_sys::qpdf_oh_get_array_item(
                        raw_doc,
                        self.obj_handle,
                        index as i32,
                    )
                })?;
        let item_object = RawObject::new(self.doc_handle.clone(), item_handle);

        Ok(Value::from_object_handle(item_object)?)
    }

    /// Puts a new item at the end of the array.
    pub fn push<V, E>(
        &mut self,
        value: V,
        warnings: &mut Warnings,
    ) -> Result<()>
    where
        V: TryInto<Value, Error = E>,
        error::Error: From<E>,
    {
        let value = value.try_into()?;

        // FIXME: Return an error if value_handle.doc_handle
        // represents a different document than self.inner.doc_handle.
        assert_eq!(value.doc_handle, self.doc_handle);

        self.doc_handle
            .call_with_warnings(warnings, |raw_doc| unsafe {
                qpdf_sys::qpdf_oh_append_item(
                    raw_doc,
                    self.obj_handle,
                    value.obj_handle,
                )
            })
    }

    /// Returns an iterator over the items of the array.
    ///
    /// Note that because this is just a handle to a PDF object,
    /// and doesn't follow Rust's ownership rules,
    /// there's nothing stopping you from cloning this object
    /// and adding or removing items while the iterator is active.
    ///
    /// Also note that the `warnings` vector is populated as the iterator runs,
    /// not just when the iterator is created.
    pub fn iter<'a>(&self, warnings: &'a mut Warnings) -> ArrayIterator<'a> {
        ArrayIterator {
            array: self.clone(),
            next_index: 0,
            warnings,
        }
    }
}

impl TryFrom<&Array> for Vec<Value> {
    type Error = error::Error;

    fn try_from(value: &Array) -> result::Result<Self, Self::Error> {
        let length =
            value.doc_handle.call_without_warnings(|raw_doc| unsafe {
                qpdf_sys::qpdf_oh_get_array_n_items(raw_doc, value.obj_handle)
            })?;

        let mut res = vec![];
        for i in 0..length {
            let item_handle =
                value.doc_handle.call_without_warnings(|raw_doc| unsafe {
                    qpdf_sys::qpdf_oh_get_array_item(
                        raw_doc,
                        value.obj_handle,
                        i,
                    )
                })?;
            let item_object =
                RawObject::new(value.doc_handle.clone(), item_handle);

            res.push(Value::from_object_handle(item_object)?);
        }

        Ok(res)
    }
}

/// The iterator returned by [`Array::iter()`].
#[derive(Debug)]
pub struct ArrayIterator<'a> {
    array: Array,
    next_index: usize,
    warnings: &'a mut Warnings,
}

impl<'a> Iterator for ArrayIterator<'a> {
    type Item = Value;

    fn next(&mut self) -> Option<Value> {
        let length = self
            .array
            .len(self.warnings)
            // We can't return the error,
            // but we can record it as a warning.
            .map_err(|e| self.warnings.push(e))
            .ok()?;

        if self.next_index >= length {
            // We're off the end of the array.
            return None;
        }

        let res = self
            .array
            .get(self.next_index, self.warnings)
            // We can't return the error,
            // but we can record it as a warning.
            .map_err(|e| self.warnings.push(e))
            .ok()?;

        self.next_index += 1;

        Some(res)
    }
}

/// A PDF object representing a map from keys to values
///
/// Unfortunately, this cannot implement
/// the [`std::ops::Index`] or [`std::ops::IndexMut`] traits
/// because they require returning a reference to a Rust type,
/// while we have to synthesise a fresh object handle.
///
/// Note that if you create a data cycle
/// (such as by inserting a dict into itself),
/// you're likely to get stack overflow crashes.
#[derive(Debug, Clone)]
pub struct Dict(RawObject);

impl Dict {
    pub fn new(doc: &mut Document, warnings: &mut Warnings) -> Result<Dict> {
        let obj_handle = doc
            .call_with_warnings(warnings, |raw_doc| unsafe {
                qpdf_sys::qpdf_oh_new_dictionary(raw_doc)
            })?;

        Ok(Dict(RawObject::new(doc.clone(), obj_handle)))
    }

    /// Return the keys in this dict
    ///
    /// ```
    /// # fn dummy() -> qpdf::Result<()> {
    /// // We always need a place to stash warnings.
    /// let mut warnings = vec![];
    /// # let mut document = qpdf::Document::open(
    /// #     "../../testdata/PDF test document 1.pdf",
    /// #     &mut warnings,
    /// # )?;
    ///
    /// // The trailer should always be a dict.
    /// let trailer: qpdf::objects::Dict =
    ///     document.trailer(&mut warnings)?.try_into()?;
    ///
    /// let keys = trailer.keys(&mut warnings)?;
    ///
    /// // NameData values can be compared with &str, so this works.
    /// assert_eq!(keys, vec!["DocChecksum", "ID", "Info", "Root", "Size"]);
    /// assert_eq!(warnings.len(), 0);
    /// # Ok(()) }
    /// # dummy().unwrap();
    /// ```
    pub fn keys(&self, warnings: &mut Warnings) -> Result<Vec<NameData>> {
        let mut res = vec![];

        self.doc_handle
            .call_with_warnings(warnings, |raw_doc| unsafe {
                qpdf_sys::qpdf_oh_begin_dict_key_iter(raw_doc, self.obj_handle)
            })?;

        while self
            .doc_handle
            .call_with_warnings(warnings, |raw_doc| unsafe {
                qpdf_sys::qpdf_oh_dict_more_keys(raw_doc)
            })?
            == qpdf_sys::QPDF_TRUE
        {
            let ptr = self
                .doc_handle
                .call_with_warnings(warnings, |raw_doc| unsafe {
                    qpdf_sys::qpdf_oh_dict_next_key(raw_doc)
                })?;

            let cstr = unsafe { ffi::CStr::from_ptr(ptr) };
            res.push(NameData { data: cstr.into() });
        }

        Ok(res)
    }

    /// Return the value associated with a given key in this dict
    ///
    /// Returns `Value::Null`
    /// if the key is not currently associated with a value in this dict,
    /// or if the asociated value is [`Null`].
    pub fn get<K, E>(&self, key: K, warnings: &mut Warnings) -> Result<Value>
    where
        K: TryInto<NameData, Error = E>,
        error::Error: From<E>,
    {
        let key = key.try_into()?;

        let raw_handle =
            self.doc_handle
                .call_with_warnings(warnings, |raw_doc| unsafe {
                    qpdf_sys::qpdf_oh_get_key(
                        raw_doc,
                        self.obj_handle,
                        key.as_ptr(),
                    )
                })?;

        let value = Value::from_object_handle(RawObject::new(
            self.doc_handle.clone(),
            raw_handle,
        ))?;

        Ok(value)
    }

    /// Set the associated value for a key in this dict
    ///
    /// Setting a key's value to [`Null`]
    /// is the same as removing the key from the dict.
    pub fn set<K, V, E, F>(
        &mut self,
        key: K,
        value: V,
        warnings: &mut Warnings,
    ) -> Result<()>
    where
        K: TryInto<NameData, Error = E>,
        V: TryInto<Value, Error = F>,
        error::Error: From<E>,
        error::Error: From<F>,
    {
        let key = key.try_into()?;
        let value = value.try_into()?;

        // FIXME: Return an error if value_handle.doc_handle
        // represents a different document than self.inner.doc_handle.
        assert_eq!(value.doc_handle, self.doc_handle);

        // FIXME: Check for an existing value,
        // so we can better mimic HashMap::insert() ?

        self.doc_handle
            .call_with_warnings(warnings, |raw_doc| unsafe {
                // This weirdly named function means:
                // "if `value` is a Null object,
                // remove `key` from the dictionary,
                // otherwise replace the current vallue associated with `key`"
                //
                // The PDF spec requires that
                // missing keys are identical to Null-valued keys,
                // so this is usually The Function You Want.
                qpdf_sys::qpdf_oh_replace_or_remove_key(
                    raw_doc,
                    self.obj_handle,
                    key.as_ptr(),
                    value.obj_handle,
                )
            })
    }
}

impl TryFrom<&Dict> for std::collections::HashMap<NameData, Value> {
    type Error = error::Error;

    fn try_from(value: &Dict) -> result::Result<Self, Self::Error> {
        use std::collections::HashMap;

        let mut res = HashMap::new();

        value.doc_handle.call_without_warnings(|raw_doc| unsafe {
            qpdf_sys::qpdf_oh_begin_dict_key_iter(raw_doc, value.obj_handle)
        })?;

        while value.doc_handle.call_without_warnings(|raw_doc| unsafe {
            qpdf_sys::qpdf_oh_dict_more_keys(raw_doc)
        })? == qpdf_sys::QPDF_TRUE
        {
            let ptr =
                value.doc_handle.call_without_warnings(|raw_doc| unsafe {
                    qpdf_sys::qpdf_oh_dict_next_key(raw_doc)
                })?;
            let k = NameData {
                data: unsafe { ffi::CStr::from_ptr(ptr).into() },
            };

            let item_handle =
                value.doc_handle.call_without_warnings(|raw_doc| unsafe {
                    qpdf_sys::qpdf_oh_get_key(
                        raw_doc,
                        value.obj_handle,
                        k.as_ptr(),
                    )
                })?;
            let v = Value::from_object_handle(RawObject::new(
                value.doc_handle.clone(),
                item_handle,
            ))?;

            res.insert(k, v);
        }

        Ok(res)
    }
}

/// A reference to another PDF object
///
/// To create an `Indirect`, use [`RawObject::make_indirect()`].
#[derive(Debug, Clone)]
pub struct Indirect(RawObject);

impl Indirect {
    /// Convert this object handle into a handle for the object it points to
    pub fn into_inner(self) -> Result<Value> {
        Value::from_direct_object_handle(self.0)
    }

    pub fn replace_target<V, E>(
        &self,
        new_target: V,
        warnings: &mut Warnings,
    ) -> Result<()>
    where
        V: TryInto<Value, Error = E>,
        error::Error: From<E>,
    {
        let new_target = new_target.try_into()?;

        // FIXME: Return an error if new_target.doc_handle
        // represents a different document than self.inner.doc_handle.
        assert_eq!(new_target.doc_handle, self.doc_handle);

        let object_num =
            self.doc_handle
                .call_with_warnings(warnings, |raw_doc| unsafe {
                    qpdf_sys::qpdf_oh_get_object_id(raw_doc, self.obj_handle)
                })?;

        let generation_num =
            self.doc_handle
                .call_with_warnings(warnings, |raw_doc| unsafe {
                    qpdf_sys::qpdf_oh_get_generation(raw_doc, self.obj_handle)
                })?;

        self.doc_handle
            .call_with_warnings(warnings, |raw_doc| unsafe {
                qpdf_sys::qpdf_replace_object(
                    raw_doc,
                    object_num,
                    generation_num,
                    new_target.obj_handle,
                )
            })
    }
}

/// A value that can be any PDF object type
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Value {
    Uninitialized(Uninitialized),
    Null(Null),
    Bool(Bool),
    Integer(Integer),
    Real(Real),
    Name(Name),
    String(String),
    Array(Array),
    Dict(Dict),
    Indirect(Indirect),
}

impl Value {
    /// Create a PDF object by parsing PDF object notation
    ///
    /// Note that this function does not support PDF syntax
    /// containing embedded NUL bytes,
    /// even though regular PDF files can include NUL bytes.
    ///
    /// ```
    /// # fn dummy() -> qpdf::Result<()> {
    /// use qpdf::objects;
    ///
    /// // We always need a place to stash warnings.
    /// let mut warnings = vec![];
    /// # let mut document = qpdf::Document::open(
    /// #     "../../testdata/PDF test document 1.pdf",
    /// #     &mut warnings,
    /// # )?;
    ///
    /// let array: objects::Array = objects::Value::from_pdf_notation(
    ///     // An array contaning the integer 42,
    ///     // the string "hello",
    ///     // and the name World.
    ///     "[ 42 (hello) /World ]",
    ///     &mut document,
    ///     &mut warnings,
    /// )?.try_into()?;
    ///
    /// let integer: objects::Integer =
    ///     array.get(0, &mut warnings)?.try_into()?;
    ///
    /// assert_eq!(integer, 42);
    ///
    /// assert_eq!(warnings.len(), 0);
    /// # Ok(()) }
    /// # dummy().unwrap();
    /// ```
    pub fn from_pdf_notation<T: Into<Vec<u8>>>(
        source: T,
        doc: &mut Document,
        warnings: &mut Warnings,
    ) -> Result<Value> {
        let source = ffi::CString::new(source).map_err(|_| error::Error {
            filename: Default::default(),
            position: 0,
            kind: error::ErrorKind::System,
            message: "Source contained NUL bytes".into(),
        })?;
        let obj_handle =
            doc.call_with_warnings(warnings, |raw_doc| unsafe {
                qpdf_sys::qpdf_oh_parse(raw_doc, source.as_ptr())
            })?;

        Value::from_object_handle(RawObject::new(doc.clone(), obj_handle))
    }

    pub(crate) fn from_object_handle(inner: RawObject) -> Result<Self> {
        // Indirect objects always report
        // the type-code of the object they point to,
        // so we have to check for "is indirect" first.
        if inner.is_indirect()? {
            Ok(Value::Indirect(Indirect(inner)))
        } else {
            Value::from_direct_object_handle(inner)
        }
    }

    fn from_direct_object_handle(inner: RawObject) -> Result<Self> {
        match inner.get_type_code()? {
            qpdf_sys::qpdf_object_type_e_ot_uninitialized => {
                Ok(Value::Uninitialized(Uninitialized(inner)))
            }
            qpdf_sys::qpdf_object_type_e_ot_null => {
                Ok(Value::Null(Null(inner)))
            }
            qpdf_sys::qpdf_object_type_e_ot_boolean => {
                Ok(Value::Bool(Bool(inner)))
            }
            qpdf_sys::qpdf_object_type_e_ot_integer => {
                Ok(Value::Integer(Integer(inner)))
            }
            qpdf_sys::qpdf_object_type_e_ot_real => {
                Ok(Value::Real(Real(inner)))
            }
            qpdf_sys::qpdf_object_type_e_ot_name => {
                Ok(Value::Name(Name(inner)))
            }
            qpdf_sys::qpdf_object_type_e_ot_string => {
                Ok(Value::String(String(inner)))
            }
            qpdf_sys::qpdf_object_type_e_ot_array => {
                Ok(Value::Array(Array(inner)))
            }
            qpdf_sys::qpdf_object_type_e_ot_dictionary => {
                Ok(Value::Dict(Dict(inner)))
            }
            code => Err(error::Error {
                kind: error::ErrorKind::Object,
                filename: Default::default(),
                position: 0,
                message: format!("Got unexpected object type code {:?}", code),
            }),
        }
    }
}

impl Deref for Value {
    type Target = RawObject;
    fn deref(&self) -> &Self::Target {
        use Value::*;
        match self {
            Uninitialized(h) => &h,
            Null(h) => &h,
            Bool(h) => &h,
            Integer(h) => &h,
            Real(h) => &h,
            Name(h) => &h,
            String(h) => &h,
            Array(h) => &h,
            Dict(h) => &h,
            Indirect(h) => &h,
        }
    }
}

macro_rules! define_conversions {
    ( $x:ident ) => {
        impl From<$x> for Value {
            fn from(value: $x) -> Value {
                Value::$x(value)
            }
        }

        impl TryFrom<Value> for $x {
            type Error = error::Error;
            fn try_from(value: Value) -> Result<$x> {
                match value {
                    Value::$x(v) => Ok(v),
                    // When $x is anything other than Indirect,
                    // this next rule allows us to automatically unwrap.
                    // an indirect wrapper.
                    // When $x is Indirect,
                    // this rule becomes unreachable as it should be -
                    // if somebody wants to unwrap a Value
                    // to get at the Indirect inside,
                    // we shouldn't automatically unwrap that Indirect.
                    #[allow(unreachable_patterns)]
                    Value::Indirect(i) => i.into_inner()?.try_into(),
                    v => Err(error::Error {
                        filename: Default::default(),
                        position: 0,
                        kind: error::ErrorKind::Object,
                        message: format!(
                            "Expected type {}, got unexpected type: {:?}",
                            stringify!($x),
                            v
                        ),
                    }),
                }
            }
        }

        impl PartialEq<$x> for $x {
            fn eq(&self, other: &$x) -> bool {
                let mut _warnings = vec![];
                let raw_self = self.unparse(&mut _warnings).ok();
                let raw_other = other.unparse(&mut _warnings).ok();

                match (raw_self, raw_other) {
                    (Some(x), Some(y)) => x == y,
                    _ => false,
                }
            }
        }
        impl Eq for $x {}

        impl From<Value> for Option<$x> {
            fn from(value: Value) -> Option<$x> {
                value.try_into().ok()
            }
        }

        impl Deref for $x {
            type Target = RawObject;
            fn deref(&self) -> &Self::Target {
                &self.0
            }
        }
    };
}

define_conversions!(Uninitialized);
define_conversions!(Null);
define_conversions!(Bool);
define_conversions!(Integer);
define_conversions!(Real);
define_conversions!(Name);
define_conversions!(String);
define_conversions!(Array);
define_conversions!(Dict);
define_conversions!(Indirect);

macro_rules! value_conversion {
    ( $x: ident, $y: ty ) => {
        impl TryFrom<Value> for $y {
            type Error = error::Error;
            fn try_from(value: Value) -> Result<$y> {
                match value {
                    Value::$x(ref x) => x.try_into(),
                    Value::Indirect(i) => i.into_inner()?.try_into(),
                    v => Err(error::Error {
                        filename: Default::default(),
                        position: 0,
                        kind: error::ErrorKind::Object,
                        message: format!(
                            "Expected type {}, got unexpected type: {:?}",
                            stringify!($x),
                            v
                        ),
                    }),
                }
            }
        }

        impl From<Value> for Option<$y> {
            fn from(value: Value) -> Option<$y> {
                value.try_into().ok()
            }
        }

        impl PartialEq<$y> for $x {
            fn eq(&self, other: &$y) -> bool {
                let converted: Option<$y> = self.try_into().ok();

                converted.map(|s| &s == other).unwrap_or(false)
            }
        }
    };
}

value_conversion!(Bool, bool);
value_conversion!(Integer, i64);
value_conversion!(Real, f64);
value_conversion!(Name, NameData);
value_conversion!(String, std::string::String);
value_conversion!(String, Vec<u8>);
value_conversion!(Array, Vec<Value>);
value_conversion!(Dict, std::collections::HashMap<NameData, Value>);

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_pdf_bool() {
        let mut doc = Document::new();
        let mut warnings = vec![];

        let true_var = Bool::new(true, &mut doc, &mut warnings)
            .expect("Could not create true bool");
        let false_var = Bool::new(false, &mut doc, &mut warnings)
            .expect("Could not create false bool");

        assert_eq!(true_var, true);
        assert_eq!(false_var, false);

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn create_pdf_integer() {
        let mut doc = Document::new();
        let mut warnings = vec![];

        let zero_var = Integer::new(0, &mut doc, &mut warnings)
            .expect("Could not create zero integer");
        let one_var = Integer::new(1, &mut doc, &mut warnings)
            .expect("Could not create one  integer");
        let big_var = Integer::new(-97, &mut doc, &mut warnings)
            .expect("Could not create big integer");

        assert_eq!(zero_var, 0i64);
        assert_eq!(one_var, 1i64);
        assert_eq!(big_var, -97i64);

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn create_pdf_real() {
        let mut doc = Document::new();
        let mut warnings = vec![];

        let zero_var = Real::new(0.0, &mut doc, &mut warnings)
            .expect("Could not create zero real");
        let one_var = Real::new(1.5, &mut doc, &mut warnings)
            .expect("Could not create one  real");
        let big_var = Real::new(-27e7, &mut doc, &mut warnings)
            .expect("Could not create big real");

        assert_eq!(zero_var, 0.0);
        assert_eq!(one_var, 1.5);
        assert_eq!(big_var, -27e7);

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn create_pdf_name() {
        let mut doc = Document::new();
        let mut warnings = vec![];

        let info_handle = Name::new("Info", &mut doc, &mut warnings)
            .expect("Could not create info name");

        let info_handle_data: NameData = (&info_handle)
            .try_into()
            .expect("Could not extract name data?");
        dbg!(info_handle_data);

        let author_handle = Name::new("Author", &mut doc, &mut warnings)
            .expect("Could not create author name");

        assert_eq!(&info_handle, "Info");
        assert_eq!(&author_handle, "Author");

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn create_pdf_string() {
        let mut doc = Document::new();
        let mut warnings = vec![];

        let text_string = "café";
        let byte_string = b"\xFE\xFF\xD8\x3D\xDC\x04"; // U+1F404 COW

        let text_handle =
            String::from_text(text_string, &mut doc, &mut warnings)
                .expect("Could not create text string");
        let bytes_handle =
            String::from_bytes(byte_string, &mut doc, &mut warnings)
                .expect("Could not create byte string");

        // Make sure we can round-trip bytes and text as-is.
        assert_eq!(bytes_handle, byte_string[0..]);
        assert_eq!(&text_handle, text_string);

        // There's only one string type in PDF, we should be able to convert
        // back and forth.
        assert_eq!(text_handle, b"caf\xE9".to_vec()); // Uses PDFDocEncoding
        assert_eq!(&bytes_handle, "🐄",);

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn create_pdf_array() {
        let mut doc = Document::new();
        let mut warnings = vec![];

        let int = Integer::new(42, &mut doc, &mut warnings)
            .expect("Could not create integer");
        let mut array = Array::new(&mut doc, &mut warnings)
            .expect("Could not create array");
        array
            .push(int, &mut warnings)
            .expect("Could not push to array");

        assert_eq!(
            array
                .len(&mut warnings)
                .expect("Could not get array length"),
            1,
        );

        let integer: i64 = array
            .get(0, &mut warnings)
            .expect("Couldn't get array value")
            .try_into()
            .expect("Got unexpected type");

        assert_eq!(integer, 42);

        array
            .get(99, &mut warnings)
            .expect_err("got value past the end of the array?");

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn create_pdf_dict() {
        let mut doc = Document::new();
        let mut warnings = vec![];

        let int = Integer::new(42, &mut doc, &mut warnings)
            .expect("Could not create integer");

        let mut dict =
            Dict::new(&mut doc, &mut warnings).expect("Could not create dict");

        dict.set("TheAnswer", int, &mut warnings)
            .expect("Could not insert into dict");

        let keys = dict.keys(&mut warnings).expect("Could not get keys");
        assert_eq!(keys, vec!["TheAnswer"]);

        let integer: i64 = dict
            .get("TheAnswer", &mut warnings)
            .expect("Could not get value")
            .try_into()
            .expect("Could not extract integer value");

        assert_eq!(integer, 42);

        let missing_value = dict
            .get("Missing", &mut warnings)
            .expect("Could not get value of missing key");

        if let Value::Null(_) = missing_value {
            // Everything's shiny
        } else {
            panic!("Got value of missing key?? {:?}", missing_value);
        }

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn dict_supports_different_key_types() {
        let mut doc = Document::new();
        let mut warnings = vec![];
        let mut dict =
            Dict::new(&mut doc, &mut warnings).expect("Could not create dict");

        // Insert a key made from a &str
        dict.set(
            "one",
            Integer::new(1, &mut doc, &mut warnings)
                .expect("Could not create integer"),
            &mut warnings,
        )
        .expect("Could not insert integer with str key");
        let new_integer: i64 = dict
            .get("one", &mut warnings)
            .expect("Could not get key one")
            .try_into()
            .expect("Could not convert value to i64");
        assert_eq!(new_integer, 1);

        // Insert a key made from a &[u8]
        let byteslice: &[u8] = b"two";
        dict.set(
            byteslice,
            Integer::new(2, &mut doc, &mut warnings)
                .expect("Could not create integer"),
            &mut warnings,
        )
        .expect("Could not insert integer with bytes key");
        let new_integer: i64 = dict
            .get("two", &mut warnings)
            .expect("Could not get key two")
            .try_into()
            .expect("Could not convert value to i64");
        assert_eq!(new_integer, 2);

        // Insert a key made from a &CStr
        let cstring =
            ffi::CString::new("three").expect("Could not create CString");
        dict.set(
            cstring.as_c_str(),
            Integer::new(3, &mut doc, &mut warnings)
                .expect("Could not create integer"),
            &mut warnings,
        )
        .expect("Could not insert integer with &CStrkey");
        let new_integer: i64 = dict
            .get("three", &mut warnings)
            .expect("Could not get key three")
            .try_into()
            .expect("Could not convert value to i64");
        assert_eq!(new_integer, 3);

        // Above, we always get a key made from a &str so let's try the others.

        // Get a key made from a &[u8]
        let new_integer: i64 = dict
            .get(byteslice, &mut warnings)
            .expect("Could not get key to")
            .try_into()
            .expect("Could not convert value to i64");
        assert_eq!(new_integer, 2);

        // Get a key made from a &CStr
        let new_integer: i64 = dict
            .get(cstring.as_c_str(), &mut warnings)
            .expect("Could not get key to")
            .try_into()
            .expect("Could not convert value to i64");
        assert_eq!(new_integer, 3);

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn make_direct_and_indirect_integer() {
        let mut doc = Document::new();
        let mut warnings = vec![];

        let integer = Integer::new(42, &mut doc, &mut warnings)
            .expect("Could not create integer");

        let indirect = integer
            .make_indirect(&mut warnings)
            .expect("Could not make indirect");

        let new_integer: i64 = indirect
            .into_inner()
            .expect("Could not convert back to Value")
            .try_into()
            .expect("Got unexpected type");

        assert_eq!(new_integer, 42i64);

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn make_direct_and_indirect_array() {
        let mut doc = Document::new();
        let mut warnings = vec![];

        // Make an array
        let mut array = Array::new(&mut doc, &mut warnings)
            .expect("Could not create array");

        // Clone it, and turn the clone into an indirect wrapper.
        let indirect = array
            .make_indirect(&mut warnings)
            .expect("Could not make indirect");

        // Insert an integer into the original array.
        let integer = Integer::new(42, &mut doc, &mut warnings)
            .expect("Could not create integer");
        array
            .push(integer, &mut warnings)
            .expect("Could not push integer into array");

        // Get an array back out of the indirect wrapper
        let new_array: Array = indirect
            .into_inner()
            .expect("Could not unwrap Value")
            .try_into()
            .expect("Got unexpected type");

        // The new array should have an item in it.
        assert_eq!(
            1,
            new_array.len(&mut warnings).expect("Could not get length")
        );

        let new_integer: Integer = new_array
            .get(0, &mut warnings)
            .expect("Could not get first item")
            .try_into()
            .expect("Got unexpected type");

        assert_eq!(new_integer, 42i64);

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn replace_indirect_target() {
        let mut doc = Document::new();
        let mut warnings = vec![];

        // Make an array
        let mut array = Array::new(&mut doc, &mut warnings)
            .expect("Could not create array");

        let integer = Integer::new(42, &mut doc, &mut warnings)
            .expect("Could not create integer");

        let indirect = integer
            .make_indirect(&mut warnings)
            .expect("Could not make indirect");

        assert_eq!(
            integer
                .unparse(&mut warnings)
                .expect("Could not unparse integer"),
            b"42",
        );
        assert_eq!(
            indirect
                .unparse(&mut warnings)
                .expect("Could not unparse integer"),
            b"1 0 R",
        );

        array
            .push(indirect.clone(), &mut warnings)
            .expect("Could not push integer into array");

        assert_eq!(
            array
                .unparse(&mut warnings)
                .expect("Could not unparse integer"),
            b"[ 1 0 R ]",
        );

        let string = String::from_text("hello", &mut doc, &mut warnings)
            .expect("Could not create new integer");

        indirect
            .replace_target(string, &mut warnings)
            .expect("Could not replace indirect target");

        // Has the integer handle been messed with?
        assert_eq!(
            integer
                .unparse(&mut warnings)
                .expect("Could not unparse integer"),
            b"42",
        );

        // The Indirect should still be indirect
        assert_eq!(
            indirect
                .unparse(&mut warnings)
                .expect("Could not unparse integer"),
            b"1 0 R",
        );

        // Now that we've replaced the target of the indirect object,
        // getting the value from the array should give us the new value,
        // not the old.
        let new_indirect: Indirect = array
            .get(0, &mut warnings)
            .expect("Could not get first item")
            .try_into()
            .expect("Got unexpected type");
        let new_string: String = new_indirect
            .into_inner()
            .expect("Could not unwrap Value")
            .try_into()
            .expect("Got unexpected type");

        assert_eq!(&new_string, "hello");

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn convert_value_directly_to_rust() {
        let mut doc = Document::new();
        let mut warnings = vec![];

        let integer = Integer::new(42, &mut doc, &mut warnings)
            .expect("Could not create integer");

        let direct_value: Value = integer.clone().into();
        assert_eq!(
            42i64,
            direct_value
                .try_into()
                .expect("Could not convert direct value to i64")
        );

        let indirect_value: Value = integer
            .make_indirect(&mut warnings)
            .expect("Could not make indirect")
            .into();
        assert_eq!(
            42i64,
            indirect_value
                .try_into()
                .expect("Could not convert indirect value to i64")
        );

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn parse_and_unparse() {
        let mut warnings = vec![];
        let mut doc = Document::new();

        Value::from_pdf_notation("invalid literal", &mut doc, &mut warnings)
            .expect_err("parsed bogus literal??");

        let integer: Integer =
            Value::from_pdf_notation("42", &mut doc, &mut warnings)
                .expect("Could not parse integer")
                .try_into()
                .expect("Could not unwrap to Integer");

        let integer_literal = integer
            .unparse(&mut warnings)
            .expect("Could not unparse integer");

        assert_eq!(integer_literal, b"42");

        // Let's create a very simple name.
        let name: Name =
            Value::from_pdf_notation("/AB", &mut doc, &mut warnings)
                .expect("Could not parse literal")
                .try_into()
                .expect("Could not unwrap to Name");
        // It should be serialised the way it was parsed.
        assert_eq!(
            name.unparse(&mut warnings).expect("Could not unparse name"),
            b"/AB",
        );

        // A name with unnecessary quoting.
        let name: Name =
            Value::from_pdf_notation("/A#42", &mut doc, &mut warnings)
                .expect("Could not parse literal")
                .try_into()
                .expect("Could not unwrap to Name");
        // It should be serialised in canonicalized form.
        assert_eq!(
            name.unparse(&mut warnings).expect("Could not unparse name"),
            b"/AB",
        );

        // A name with necessary quoting.
        let name: Name =
            Value::from_pdf_notation("/A#20B", &mut doc, &mut warnings)
                .expect("Could not parse literal")
                .try_into()
                .expect("Could not unwrap to Name");
        // It should be serialised the way it was parsed.
        assert_eq!(
            name.unparse(&mut warnings).expect("Could not unparse name"),
            b"/A#20B",
        );

        // A name cannot contain NUL, even when quoted.
        let error = Value::from_pdf_notation("/A#00B", &mut doc, &mut warnings)
            .expect_err("parsed broken Name?");

        assert_eq!(error.message, "null character not allowed in name token");

        // A name can contain non-whitespace, non-UTF-8 bytes.
        let name: Name =
            Value::from_pdf_notation(&b"/A\xffB"[..], &mut doc, &mut warnings)
                .expect("Could not parse literal")
                .try_into()
                .expect("Could not unwrap to Name");
        // For safety, bytes are escaped when canonicalized.
        assert_eq!(
            name.unparse(&mut warnings).expect("Could not unparse name"),
            b"/A#ffB",
        );

        for c in u8::MIN..=u8::MAX {
            let raw_name_bytes: Vec<u8> = vec![b'/', c];
            let encoded_name_bytes: Vec<u8> = format!("/#{:02x}", c).into();

            let raw_name_obj: Option<Name> = Value::from_pdf_notation(
                &raw_name_bytes[..],
                &mut doc,
                &mut warnings,
            )
            .ok()
            .and_then(|v| v.into());

            let canonicalized_raw_name = raw_name_obj
                .map(|v| v.unparse(&mut warnings))
                .transpose()
                .expect("Could not unparse parsed Name");

            let encoded_name_obj: Option<Name> = Value::from_pdf_notation(
                &encoded_name_bytes[..],
                &mut doc,
                &mut warnings,
            )
            .ok()
            .and_then(|v| v.into());

            let canonicalized_encoded_name = encoded_name_obj
                .map(|v| v.unparse(&mut warnings))
                .transpose()
                .expect("Could not unparse parsed Name");

            match c {
                0x00 => {
                    // Cannot be used in a name, encoded or otherwise.
                    assert_eq!(canonicalized_raw_name, None);
                    assert_eq!(canonicalized_encoded_name, None);
                }
                0x09..=0x0D | 0x20 => {
                    // Whitespace control characters are not part of the name,
                    // but can be used escaped.
                    assert_eq!(canonicalized_raw_name, Some(vec![b'/']));
                    assert_eq!(
                        canonicalized_encoded_name,
                        Some(encoded_name_bytes)
                    );
                }
                b'#'          // Used in escaping names
                | b'%'        // Comments
                | b'('..=b')' // Strings
                | b'/'        // Names
                | b'<'        // Dicts and Strings
                | b'>'
                | b'['        // Arrays
                | b']'
                | b'{'        // PostScript functions??
                | b'}' => {
                    // PDF syntax tokens can't be used raw,
                    // but can be used escaped.
                    assert_eq!(canonicalized_raw_name, None);
                    assert_eq!(
                        canonicalized_encoded_name,
                        Some(encoded_name_bytes)
                    );
                }
                0x21..=0x7E => {
                    // Printable ASCII characters can be used as-is,
                    // and will not be encoded.
                    assert_eq!(
                        canonicalized_raw_name,
                        Some(raw_name_bytes.clone())
                    );
                    assert_eq!(
                        canonicalized_encoded_name,
                        Some(raw_name_bytes)
                    );
                }
                0x01..=u8::MAX => {
                    // Non-whitespace control characters,
                    // and non-ASCII characters,
                    // can be used as-is but should be encoded.
                    assert_eq!(
                        canonicalized_raw_name,
                        Some(encoded_name_bytes.clone())
                    );
                    assert_eq!(
                        canonicalized_encoded_name,
                        Some(encoded_name_bytes)
                    );
                }
            }
        }
    }
}
