//! Errors and warnings returned by QPDF
//!
//! QPDF reports various fields for each error or warning it detects,
//! which are here collected in the [`Error`] type.
use std::error;
use std::ffi;
use std::fmt;

/// All the kinds of errors and warnings QPDF can produce
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Copy, Clone)]
#[repr(u32)]
#[non_exhaustive]
pub enum ErrorKind {
    /// Everything's fine
    Success = qpdf_sys::qpdf_error_code_e_qpdf_e_success,
    /// Logic/programming error -- indicates bug in QPDF
    Internal = qpdf_sys::qpdf_error_code_e_qpdf_e_internal,
    /// I/O error, memory error, etc.
    System = qpdf_sys::qpdf_error_code_e_qpdf_e_system,
    /// PDF feature not (yet) supported by QPDF
    Unsupported = qpdf_sys::qpdf_error_code_e_qpdf_e_unsupported,
    /// Incorrect password for encrypted file
    Password = qpdf_sys::qpdf_error_code_e_qpdf_e_password,
    /// Syntax errors or other damage in PDF
    DamagedPdf = qpdf_sys::qpdf_error_code_e_qpdf_e_damaged_pdf,
    /// Erroneous or unsupported pages structure
    Pages = qpdf_sys::qpdf_error_code_e_qpdf_e_pages,
    /// Type/bounds errors accessing objects
    Object = qpdf_sys::qpdf_error_code_e_qpdf_e_object,
    /// QPDF returned an error code the Rust wrapper doesn't recognise
    Unrecognised,
}

/// An error or warning produced by QPDF
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Error {
    /// The general category of problem this error or warning represents
    pub kind: ErrorKind,
    /// If non-empty, the problem occurred while processing the named file
    pub filename: ffi::CString,
    /// The problem occurred at this byte offset into the given file
    pub position: usize,
    /// A human-readable description of the problem
    pub message: String,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{filename:?}:{position}: {kind:?}: {message}",
            filename = self.filename,
            position = self.position,
            kind = self.kind,
            message = self.message,
        )
    }
}

impl error::Error for Error {}
impl From<std::convert::Infallible> for Error {
    fn from(_: std::convert::Infallible) -> Self {
        // It's impossible to create an Infallible,
        // therefore we should never be asked to convert one.
        unreachable!();
    }
}

pub type Result<T> = std::result::Result<T, Error>;
pub type Warnings = Vec<Error>;
