//! Rust bindings for the QPDF library
//!
//! This library provides a (more) idiomatic Rust API
//! for the [QPDF] library.
//!
//! Currently, it is based on the limited C-based API QPDF provides
//! (not the richer C++ API)
//! and it doesn't even handle all of that,
//! but at least it's a start.
//!
//! Quirks
//! ======
//!
//! Because this is a Rust API wrapping a C API wrapping a C++ API,
//! there's some unavoidable weirdness.
//!
//! Error handling
//! --------------
//!
//! According to the API documentation,
//! any QPDF function can produce an error,
//! and always produces zero or more warnings.
//! Therefore,
//! every [`Document`] method
//! returns a [`Result`] in case an error is detected,
//! and also takes a `&mut` [`Warnings`]
//! (that is, a `&mut Vec<error::Error>`)
//! to record any recoverable problems that occured.
//! You can share the same `Warnings` across multiple calls
//! to collect all the warnings in a larger operation,
//! or just toss them all away if you don't care about them.
//!
//! These warnings and errors are stored ine QPDF document object,
//! and reading them is destructive.
//! As a result,
//! every method on [`Document`]
//! (even the ones that seem purely informational)
//! requires a mutable reference (`&mut`) to the `Document` object.
//!
//! Filenames
//! ---------
//!
//! QPDF uses UTF-8 strings for filenames,
//! so this wrapper uses Rust strings which are guaranteed to be UTF-8.
//! Actually, QPDF does something very similar to Rust's `OsString`,
//! where filenames are kept as UTF-8 and transcoded to UTF-16 on Windows,
//! but Rust's `OsStr` on Windows
//! does not expose the UTF-8 data QPDF wants,
//! so we can't use Rust-native types for filenames.
//! We could use Rust to read the file-data from disk
//! and present QPDF with an anonymous buffer to parse,
//! but QPDF can read on-disk files much more efficiently
//! than loading the whole thing into memory.
//!
//! [QPDF]: https://qpdf.sourceforge.io/
extern crate qpdf_sys;

use std::cell;
use std::ffi;
use std::rc;
use std::str;

use std::result;

// XXX: Should I use qpdf_get_last_string_length()
// for *every* API call that returns a string,
// or only for PDF object calls?

/// Describes the version of the QPDF library in use.
///
/// Should be a string like `"1.2.3"`.
///
/// Returns [std::str::Utf8Error] if the library returns invalid data.
///
/// Example
/// -------
///
/// ```
/// # fn dummy() -> std::result::Result<(), std::str::Utf8Error> {
/// let version = qpdf::get_version()?;
/// println!("Currently using QPDF version v{}", version);
/// # Ok(()) }
/// ```
pub fn get_version() -> result::Result<String, str::Utf8Error> {
    unsafe {
        Ok(std::ffi::CStr::from_ptr(qpdf_sys::qpdf_get_qpdf_version())
            .to_str()?
            .to_owned())
    }
}

pub mod error;
pub mod keys;
pub mod objects;

pub use error::Error;
pub use error::Result;
pub use error::Warnings;

/// A Rust wrapper around QPDF's document handle type
///
/// This is a separate struct so that we can implement traits like Drop
/// directly, inside the reference counting we want to use.
#[derive(Debug, PartialEq, Eq)]
struct RawDocument {
    inner: qpdf_sys::qpdf_data,
}

impl Drop for RawDocument {
    fn drop(&mut self) {
        unsafe {
            qpdf_sys::qpdf_cleanup(&mut self.inner);
        }
    }
}

impl RawDocument {
    fn new() -> RawDocument {
        let inner = unsafe { qpdf_sys::qpdf_init() };
        unsafe {
            // We want to report errors and warnings via our API,
            // not write them to stderr.
            qpdf_sys::qpdf_silence_errors(inner);
            qpdf_sys::qpdf_set_suppress_warnings(inner, qpdf_sys::QPDF_TRUE);
        }
        Self { inner }
    }

    fn extract_current_error(
        &mut self,
        handle: qpdf_sys::qpdf_error,
    ) -> error::Error {
        let raw_kind =
            unsafe { qpdf_sys::qpdf_get_error_code(self.inner, handle) };
        let kind = match raw_kind {
            qpdf_sys::qpdf_error_code_e_qpdf_e_success => {
                error::ErrorKind::Success
            }
            qpdf_sys::qpdf_error_code_e_qpdf_e_internal => {
                error::ErrorKind::Internal
            }
            qpdf_sys::qpdf_error_code_e_qpdf_e_system => {
                error::ErrorKind::System
            }
            qpdf_sys::qpdf_error_code_e_qpdf_e_unsupported => {
                error::ErrorKind::Unsupported
            }
            qpdf_sys::qpdf_error_code_e_qpdf_e_password => {
                error::ErrorKind::Password
            }
            qpdf_sys::qpdf_error_code_e_qpdf_e_damaged_pdf => {
                error::ErrorKind::DamagedPdf
            }
            qpdf_sys::qpdf_error_code_e_qpdf_e_pages => error::ErrorKind::Pages,
            qpdf_sys::qpdf_error_code_e_qpdf_e_object => {
                error::ErrorKind::Object
            }
            _ => error::ErrorKind::Unrecognised,
        };
        let filename = unsafe {
            ffi::CStr::from_ptr(qpdf_sys::qpdf_get_error_filename(
                self.inner, handle,
            ))
            .to_owned()
        };
        let position = unsafe {
            qpdf_sys::qpdf_get_error_file_position(self.inner, handle) as usize
        };
        let message = unsafe {
            ffi::CStr::from_ptr(qpdf_sys::qpdf_get_error_message_detail(
                self.inner, handle,
            ))
            .to_string_lossy()
            .to_string()
        };

        error::Error {
            kind,
            filename,
            position,
            message,
        }
    }

    fn check_warnings(&mut self, warnings: &mut Warnings) {
        while unsafe {
            qpdf_sys::qpdf_more_warnings(self.inner) == qpdf_sys::QPDF_TRUE
        } {
            let handle = unsafe { qpdf_sys::qpdf_next_warning(self.inner) };
            warnings.push(self.extract_current_error(handle));
        }
    }

    fn check_error(&mut self) -> Result<()> {
        if unsafe { qpdf_sys::qpdf_has_error(self.inner) }
            == qpdf_sys::QPDF_FALSE
        {
            return Ok(());
        }

        let handle = unsafe { qpdf_sys::qpdf_get_error(self.inner) };

        Err(self.extract_current_error(handle))
    }

    fn check_results(&mut self, warnings: &mut Warnings) -> Result<()> {
        self.check_warnings(warnings);
        self.check_error()
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Document {
    handle: rc::Rc<cell::RefCell<RawDocument>>,
}

impl Document {
    // FIXME: As of QPDF 10.6,
    // we can use qpdf_empty_pdf() to get an empty PDF skeleton
    // and then we can use it to make this method public.
    #[cfg(test)]
    fn new() -> Self {
        let handle = RawDocument::new();

        Document {
            handle: rc::Rc::new(cell::RefCell::new(handle)),
        }
    }
    /// Open a PDF file and read it into memory
    ///
    /// `filename` is the filesystem path of a PDF file to read.
    ///
    /// `password` is the password used to decrypt the PDF.
    /// If the  file is not encrypted,
    /// pass an empty string.
    ///
    /// `warnings` will be filled with non-fatal problems (if any)
    /// encountered while reading the file.
    /// If you want to collect warnings for a whole series of operations,
    /// you can pass the same `Vec` each time
    /// to collect them all in the same place.
    /// If you don't care about collecting warnings,
    /// you can pass a different `Vec` each time and throw it away.
    ///
    /// Example
    /// -------
    ///
    /// ```
    /// # fn dummy() -> qpdf::Result<()> {
    /// let mut warnings = vec![];
    /// let document = qpdf::Document::open(
    ///     "somefile.pdf",
    ///     &mut warnings,
    /// )?;
    ///
    /// for each in warnings {
    ///     eprintln!("somefile has a problem: {}", each);
    /// }
    /// # Ok(()) }
    /// ```
    pub fn open(filename: &str, warnings: &mut Warnings) -> Result<Document> {
        let res = Document {
            handle: rc::Rc::new(cell::RefCell::new(RawDocument::new())),
        };
        let path = ffi::CString::new(filename).map_err(|_| Error {
            filename: Default::default(),
            position: 0,
            kind: error::ErrorKind::System,
            message: format!(
                "Could not convert filename to CString: {:?}",
                filename
            ),
        })?;
        let password: ffi::CString = Default::default();

        res.call_with_warnings(warnings, |raw_doc| unsafe {
            qpdf_sys::qpdf_read(raw_doc, path.as_ptr(), password.as_ptr());
        })?;
        res.call_with_warnings(warnings, |raw_doc| unsafe {
            qpdf_sys::qpdf_check_pdf(raw_doc);
        })?;

        Ok(res)
    }

    pub fn write(
        &mut self,
        filename: &str,
        warnings: &mut Warnings,
    ) -> Result<()> {
        let path = ffi::CString::new(filename).map_err(|_| Error {
            filename: Default::default(),
            position: 0,
            kind: error::ErrorKind::System,
            message: format!(
                "Could not convert filename to CString: {:?}",
                filename
            ),
        })?;

        self.call_with_warnings(warnings, |raw_doc| {
            unsafe { qpdf_sys::qpdf_init_write(raw_doc, path.as_ptr()) };
        })?;
        self.call_with_warnings(warnings, |raw_doc| {
            unsafe { qpdf_sys::qpdf_write(raw_doc) };
        })?;

        Ok(())
    }

    /// Returns the version of the PDF specification this document uses
    ///
    /// `warnings` will be filled with non-fatal problems (if any)
    /// encountered during the operation.
    ///
    /// Example
    /// -------
    ///
    /// ```
    /// # fn dummy() -> qpdf::Result<()> {
    /// let mut warnings = vec![];
    ///
    /// let mut document = qpdf::Document::open(
    ///     "somefile.pdf",
    ///     &mut warnings,
    /// )?;
    ///
    /// let version = document.get_pdf_version(&mut warnings)?;
    ///
    /// println!("Document uses PDF v{}", version);
    ///
    /// # Ok(()) }
    /// ```
    pub fn get_pdf_version(&self, warnings: &mut Warnings) -> Result<String> {
        let cstr = unsafe {
            std::ffi::CStr::from_ptr(
                self.call_with_warnings(warnings, |raw_doc| {
                    qpdf_sys::qpdf_get_pdf_version(raw_doc)
                })?,
            )
        };
        // XXX: Does extracting errors invalidate the PDF version string buffer?
        Ok(cstr
            .to_str()
            .expect("qpdf should have reported an invalid version string")
            .to_string())
    }

    /// Returns the extension level this document uses.
    ///
    /// This defines what Adobe-proprietary features the document uses,
    /// beyond those defined in the PDF 1.7/ISO-32000-1 specification.
    ///
    /// `warnings` will be filled with non-fatal problems (if any)
    /// encountered during the operation.
    ///
    /// Example
    /// -------
    ///
    /// ```
    /// # fn dummy() -> qpdf::Result<()> {
    /// let mut warnings = vec![];
    ///
    /// let mut document = qpdf::Document::open(
    ///     "somefile.pdf",
    ///     &mut warnings,
    /// )?;
    ///
    /// let extension_level = document.get_pdf_extension_level(&mut warnings)?;
    ///
    /// println!("Document uses extension level {}", extension_level);
    ///
    /// # Ok(()) }
    /// ```
    pub fn get_pdf_extension_level(
        &mut self,
        warnings: &mut Warnings,
    ) -> Result<i32> {
        let res = self.call_with_warnings(warnings, |raw_doc| unsafe {
            qpdf_sys::qpdf_get_pdf_extension_level(raw_doc)
        })?;
        Ok(res)
    }

    /// Retrieve the trailer from the PDF document
    ///
    /// The trailer should be a [`objects::Dict`]
    /// that provides access to all the document's data.
    pub fn trailer(&self, warnings: &mut Warnings) -> Result<objects::Value> {
        let raw_obj_handle = self
            .call_with_warnings(warnings, |raw_doc| unsafe {
                qpdf_sys::qpdf_get_trailer(raw_doc)
            })?;

        Ok(objects::Value::from_object_handle(
            objects::RawObject::new(self.clone(), raw_obj_handle),
        )?)
    }

    fn call_with_warnings<T, F>(
        &self,
        warnings: &mut Warnings,
        closure: F,
    ) -> Result<T>
    where
        F: FnOnce(qpdf_sys::qpdf_data) -> T,
    {
        let mut raw_doc = self.handle.borrow_mut();
        let res = closure(raw_doc.inner);
        raw_doc.check_results(warnings)?;
        Ok(res)
    }

    fn call_without_warnings<T, F>(&self, closure: F) -> Result<T>
    where
        F: FnOnce(qpdf_sys::qpdf_data) -> T,
    {
        let mut raw_doc = self.handle.borrow_mut();
        let res = closure(raw_doc.inner);
        raw_doc.check_error()?;
        Ok(res)
    }
}

#[cfg(test)]
mod tests {
    extern crate tempfile;
    use super::*;

    #[test]
    fn open_bogus_filename() {
        let mut warnings = vec![];
        let error = Document::open("bogus.pdf", &mut warnings)
            .expect_err("opened a bogus PDF?");

        println!("Got failure: {:#?}", error);

        assert_eq!(warnings, vec![]);
        assert_eq!(error.kind, error::ErrorKind::System);
        assert_eq!(error.position, 0);
        assert!(error.message.contains("bogus.pdf"));
    }

    #[test]
    fn open_real_file() {
        let mut warnings = vec![];
        let mut doc = Document::open(
            "../../testdata/PDF test document 1.pdf",
            &mut warnings,
        )
        .expect("Could not open real document");

        let version = doc
            .get_pdf_version(&mut warnings)
            .expect("Got gibberish version?");
        assert_eq!(version, "1.6");

        let extension_level = doc
            .get_pdf_extension_level(&mut warnings)
            .expect("Got invalid extensions level?");
        assert_eq!(extension_level, 0);

        let trailer: objects::Dict = doc
            .trailer(&mut warnings)
            .expect("Could not get trailer")
            .try_into()
            .expect("Trailer was not a dict");

        let keys = trailer.keys(&mut warnings).expect("Could not get keys");
        println!("Trailer has keys: {:#?}", keys);

        let size: i64 = trailer
            .get("Size", &mut warnings)
            .expect("Could not get Size from trailer dict")
            .try_into()
            .expect("Got unexpected size type");

        assert_eq!(25i64, size);

        assert_eq!(warnings.len(), 0);
    }

    #[test]
    fn copy_document_to_new_file() {
        let mut warnings = vec![];
        let mut doc = Document::open(
            "../../testdata/PDF test document 1.pdf",
            &mut warnings,
        )
        .expect("Could not open real document");

        let tempdir =
            tempfile::tempdir().expect("Could not create temporary dir");

        let target_path = tempdir.path().join("target.pdf");
        let target_str = target_path
            .to_str()
            .expect("Could not convert path to UTF-8");

        doc.write(target_str, &mut warnings)
            .expect("Could not write document to disk");

        assert_eq!(warnings.len(), 0);
    }
}
