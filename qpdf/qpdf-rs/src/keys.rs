//! Constants commonly used as keys in PDF files
//!
//! Although you can typically use a string literal
//! wherever QPDF wants a dictionary key,
//! there's a chance you might make a typo
//! if you have to refer to the same key a number of times.
//! Use the constants in this file,
//! and then the compiler should warn you if you type one wrong!

// Common keys
pub const TYPE: &str = "Type";
pub const PARENT: &str = "Parent";
pub const KIDS: &str = "Kids";
pub const COUNT: &str = "Count";
pub const NAMES: &str = "Names";
pub const NUMS: &str = "Nums";
pub const LIMITS: &str = "Limits";

// Trailer keys
pub const SIZE: &str = "Size";
pub const PREV: &str = "Prev";
pub const ROOT: &str = "Root";
pub const ENCRYPT: &str = "Encrypt";
pub const INFO: &str = "Info";
pub const ID: &str = "ID";

// Info keys
pub const TITLE: &str = "Title";
pub const AUTHOR: &str = "Author";
pub const SUBJECT: &str = "Subject";
pub const KEYWORDS: &str = "Keywords";
pub const CREATOR: &str = "Creator";
pub const PRODUCER: &str = "Producer";
pub const CREATION_DATE: &str = "CreationDate";
pub const MOD_DATE: &str = "ModDate";
pub const TRAPPED: &str = "Trapped";

// Root keys
pub const CATALOG: &str = "Catalog";
pub const VERSION: &str = "Version";
pub const EXTENSIONS: &str = "Extensions";
pub const PAGES: &str = "Pages";
pub const PAGE_LABELS: &str = "PageLabels";
// Names is a common key
pub const DESTS: &str = "Dests";
pub const VIEWER_PREFERENCES: &str = "ViewerPreferences";
pub const PAGE_LAYOUT: &str = "PageLayout";
pub const PAGE_MODE: &str = "PageMode";
pub const OUTLINES: &str = "Outlines";
pub const THREADS: &str = "Threads";
pub const OPEN_ACTION: &str = "OpenAction";
pub const AA: &str = "AA";
pub const URI: &str = "URI";
pub const ACRO_FORM: &str = "AcroForm";
pub const META_DATA: &str = "MetaData";
pub const STRUCT_TREE_ROOT: &str = "StructTreeRoot";
pub const MARK_INFO: &str = "MarkInfo";
pub const LANG: &str = "Lang";
pub const SPIDER_INFO: &str = "SpiderInfo";
pub const OUTPUT_INTENTS: &str = "OutputIntents";
pub const PIECE_INFO: &str = "PieceInfo";
pub const OC_PROPERTIES: &str = "OCProperties";
pub const PERMS: &str = "Perms";
pub const LEGAL: &str = "Legal";
pub const REQUIREMENTS: &str = "Requirements";
pub const COLLECTION: &str = "Collection";
pub const NEEDS_RENDERING: &str = "NeedsRendering";
