#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

pub const QPDF_FALSE: i32 = 0;
pub const QPDF_TRUE: i32 = 1;

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let version;
        unsafe {
            version = std::ffi::CStr::from_ptr(qpdf_get_qpdf_version())
                .to_str()
                .expect("qpdf version not UTF-8 safe?");
        }

        println!("Got version string {0:?}", version);

        // We expect the version string to be something like "1.2.3",
        // except of course we don't want to hard-code a specific version.
        assert!(version.len() > 0, "version should not be empty");

        for part in version.split(".") {
            assert!(
                part.len() > 0,
                "version should not contain consecutive dots",
            );

            for c in part.chars() {
                assert!(
                    c.is_ascii_digit(),
                    "version parts must be ASCII decimal numbers",
                );
            }
        }
    }
}
