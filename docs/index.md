PDF Patchup
===========

A tool to annotate and augment PDF files.

  - Update built-in title/author/keyword metadata
  - Make PDF page numbering match the printed page numbers

PDF files are a great way to publish documents online,
with many features to help readers navigate and annotate the text.
However,
while some creators go to great effort
to exploit all the potential of the PDF format,
and to collect and issue errata and updates,
not every creator has the time and resources to do those things.
For a very popular creator,
fans might step up to collect and publish their own notes,
but that's another separate publication to track and keep up to date;
copyright law prevents fans from distributing
polished and enhanced copies of the original document.

PDF Patchup lets fans create a patch
that describes how to improve a particular PDF,
and lets end-users apply that patch to a PDF they have legally acquired.
The patch should not be copyright encumbered,
since it does not contain the original author's copyrighted content,
so fans can collaborate on improvements and polish
and the end result is a customised version of the original PDF
instead of a separate document.
