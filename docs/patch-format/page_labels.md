`page_labels` Section
=====================

The `page_labels` section
controls how the the pages of a PDF file are numbered.

Normally,
a PDF's pages are numbered from 1 to however many pages there are,
but if it represents a digital copy of a physical document,
those "natural" page numbers can be misleading.
For example,
if a PDF represents a single article or chapter
from the middle of a larger work,
the page numbers might start much higher.
Alternatively,
if the PDF represents an entire book,
it might include the cover, title page, or table of contents,
which traditionally don't get numbers at all.

Page labels allow the PDF's page numbering
to match the numbers printed on the pages,
so if the text says "see page 123 for details"
and you tell your PDF reader to jump to page 123,
you wind up in the right place.

Since page numbers are quite predictable,
we don't need to store a separate page number for every PDF page.
Instead,
we just record when the numbering pattern changes.
In a patch file,
rather than have a single `page_labels` section,
we have multiple `page_labels.X` sections
where `X` is the "real" page number
where the new style begins.

For example:

```toml
[page_labels.1]
prefix = "Cover"

[page_labels.2]
style = "LowercaseRoman"

[page_labels.6]
style = "Decimal"
```

This patch represents the following sequence of page labels:

Page Number | Page Label
----------- | ----------
1           | Cover
2           | i
3           | ii
4           | iii
5           | iv
6           | 1
7           | 2
8           | 3
...         | ...

This might represent a PDF with
a cover,
four pages of front matter (title page, table of contents, etc.)
and then regular page numbering after that.

Because of the way TOML syntax works,
you could also write the above labelling configuration
using "inline sections", like this:

```toml
[page_labels]
1 = { prefix = "Cover" }
2 = { style = "LowercaseRoman" }
6 = { style = "Decimal" }
```

This works exactly the same way, and is more compact,
but the "inline section" syntax is more complex,
so you can use it or not as you prefer.

If the first `page_labels` section does not describe page number 1,
the pages before the first section are labelled
as though there was a section like this:

```toml
[page_labels.1]
style = "Decimal"
```

If you want your patch to remove all the page label configuration from a PDF,
you can just make a `page_labels` section with no inline sections,
like this:

```toml
[page_labels]
# This removes all custom page labelling from the PDF
```

That way *all* pages will be labelled in the default,
unprefixed decimal style.

`prefix` Setting
----------------

Type
: Text

Required
: No, default ""

If present,
the following pages' labels will begin with the prefix text.
Otherwise,
the labels will have no prefix.

For example,
if the first page of the PDF is cover art and doesn't need a number:

```toml
[page_labels.1]
prefix = "Cover"
```

If you want Appendix A's pages to be numbered
"A-1", "A-2", "A-3", and so forth:

```toml
[page_labels.237] # or whatever page Appendix A actually starts on
prefix = "A-"
style = "Decimal"
```


`style` Setting
---------------

Type
: Text

Required
: No

If present,
the following pages' labels will end
with a number in the given style.

Must be one of the following keywords:

Keyword | Result
------- | -------
Decimal | 1, 2, 3, 4, ...
UppercaseRoman | I, II, III, IV, ...
LowercaseRoman | i, ii, iii, iv, ...
UppercaseLetters | A, B, C, D, ...
LowercaseLetters | a, b, c, d, ...

If not provided,
the following pages' labels will have no numeric component.

`start` Setting
---------------

Type
: Integer > 0

Required
: No, default 1

If present, the following pages' labels
will start from the given number
(formatted according to [`style`](#style-setting), if any).

For example,
if a PDF represents an article
originally published on pages 36-41 of a magazine,
you could make the PDF's page numbers start at 36:

```toml
[page_labels.1]
start = 36
```

If not present,
the following pages' labels will start at 1
(formatted according to [`style`](#style-setting), if any).
