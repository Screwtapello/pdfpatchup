`metadata` Section
==================

The `metadata` section
tells PDF Patchup how to modify the PDF's internal metadata:
the author, title, keywords, etc.
All the settings in this section are interpreteted the same way:

  - An omitted setting means the corresponding metadata field is left alone
  - A setting with a text value means the corresponding metadata field
    will be set to that value.
  - A setting with an empty value means the corresponding metadata field
    will be cleared.

For example, imagine a PDF with the following metadata:

Author
: A. Writer

Title
: My Fabulous Manuscript

Subject
: A book about how wonderful the author's handwriting is

Let's say we apply the following patch:

```toml
[metadata]
# author setting is not provided
title = "This is a better title"
subject = ""
```

The resulting PDF's metadata would look like this:

Author
: A. Writer

Title
: This is a better title

Subject
: *None*

That is, the "Author" field would be left alone,
the "Title" field would be changed,
and the "Subject" field would be cleared.


`author` Setting
----------------

Type
: Text

Required
: No

If set to non-empty text,
sets the Author metadata field of the PDF file.

If set to empty text,
the metadata field will be cleared.

The Author field represents the name of the person or people
who created the document.

`title` Setting
---------------

Type
: Text

Required
: No

If set to non-empty text,
sets the Title metadata field of the PDF file.

If set to empty text,
the metadata field will be cleared.

The Title field represents
the document's title.

`producer` Setting
------------------

Type
: Text

Required
: No

If set to non-empty text,
sets the Producer metadata field of the PDF file.

If set to empty text,
the metadata field will be cleared.

The Producer field represents
the software that converted the document to PDF,
if it was originally in a different format.

`creator` Setting
-----------------

Type
: Text

Required
: No

If set to non-empty text,
sets the Creator metadata field of the PDF file.

If set to empty text,
the metadata field will be cleared.

The Creator field represents
the software that created the document that was converted to PDF,
if it was originally in a different format.

`creation_date` Setting
-----------------------

Type
: Text

Required
: No

If set to non-empty text,
sets the Creation Date metadata field of the PDF file.

If set to empty text,
the metadata field will be cleared.

The Creation Date field represents
the date and time the document was originally created.
It must be in the format described
in section 7.9.4 of the PDF 1.7 specification.

`modification_date` Setting
---------------------------

Type
: Text

Required
: No

If set to non-empty text,
sets the Modification Date metadata field of the PDF file.

If set to empty text,
the metadata field will be cleared.

The Modification Date field represents
the date and time the document was most recently modified.

It must be in the format described
in section 7.9.4 of the PDF 1.7 specification.

`subject` Setting
-----------------

Type
: Text

Required
: No

If set to non-empty text,
sets the Subject metadata field of the PDF file.

If set to empty text,
the metadata field will be cleared.

The Subject field is a human-readable description
of the document's subject.

`keywords` Setting
------------------

Type
: Text

Required
: No

If set to non-empty text,
sets the Keywords metadata field of the PDF file.

If set to empty text,
the metadata field will be cleared.

The Keywords field is a machine-readable list
of keywords relevant to a document's content,
to help decide whether a particular document is relevant
to a particular search query.
Keywords are usually separated by commas (`,`) or semicolons (`;`).
