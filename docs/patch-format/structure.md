Structure
=========

A `.pdfpatch` file is a plain text file
you can edit with any plain text editor,
like Notepad on Windows,
or TextEdit on macOS.

It has sections and settings
based on the [TOML] v1.0 syntax,
but you shouldn't need to know all the details of that format
to use this one.

[TOML]: https://toml.io/

Sections
--------

The file is broken up into *sections*,
which begin with a *header* in square brackets.
For example,
here's the header for the `recogniser` section:

```toml
[recogniser]
```

Settings
--------

After each header,
there can be zero or more *settings*,
which have a *name*,
an equals sign (`=`),
and a *value*.
For example,
these settings
set `page_count` to the number 2,
and `title` to the text "PDF test":

```toml
page_count = 2
title = "PDF test"
```

The next pages describe what sections PDF Patchup uses,
and what settings are allowed in each section.
