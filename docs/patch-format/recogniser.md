`recogniser` Section
====================

When PDF Patchup is given a PDF file
and needs to find which of a collection of patches can apply to it,
it uses the `recogniser` section of each patch
to see whether the patch "recognises" the PDF.

Each setting in this section describes one aspect of a PDF file.
If the PDF matches the description,
this patch gets a point.
After the each patch has had a chance to recognise the PDF,
the patch with the highest number of points will be chosen.

None of these settings are required,
any that are missing just won't contribute to the patche's point total.

Example:

```toml
[recogniser]
page_count = 2
format = "PDF 1.7"
author = "Lewis Caroll"
title = "Alice's Adventures in Wonderland"
producer = "Acrobat Distiller 8.1.0 (Windows)"
creator = "Microsoft Word 7.0"
subject = "A fantasy exploration through a world of anthropomorphic creatures"
keywords = "Alice, Wonderland, White Rabbit, Caterpilla, Queen of Hearts"
```

`page_count` Setting
--------------------

Type
: Integer

Required
: No

Increases the match score of this patch
if the target PDF's page count is exactly this number.

`format` Setting
----------------

Type
: Text

Required
: No

Increases the match score of this patch
if the target PDF uses exactly this version of the PDF format.

`author` Setting
----------------

Type
: Text

Required
: No

Increases the match score of this patch
if the target PDF's Author metadata is set to this exact text.

`title` Setting
---------------

Type
: Text

Required
: No

Increases the match score of this patch
if the target PDF's Title metadata is set to this exact text.

`producer` Setting
------------------

Type
: Text

Required
: No

Increases the match score of this patch
if the target PDF's Producer metadata is set to this exact text.

`creator` Setting
-----------------

Type
: Text

Required
: No

Increases the match score of this patch
if the target PDF's Creator metadata is set to this exact text.

`subject` Setting
-----------------

Type
: Text

Required
: No

Increases the match score of this patch
if the target PDF's Subject metadata is set to this exact text.

`keywords` Setting
------------------

Type
: Text

Required
: No

Increases the match score of this patch
if the target PDF's Keywords metadata is set to this exact text.
